package main.sopianaiskandar.net.lubricantssales;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import main.sopianaiskandar.net.lubricantssales.adapter.SessionManager;

/**
 * Created by sopianaiskandar on 2/11/16.
 */
public class RouteActivity extends AppCompatActivity {
    private SessionManager session;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        session = new SessionManager(getApplicationContext());

        if(session.isLoggedIn()){
            Intent intent = new Intent(this,HomeActivity.class);
            startActivity(intent);
            finish();
        }else {
            Intent intent = new Intent(this,LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }
}
