package main.sopianaiskandar.net.lubricantssales.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
/**
 * Created by sopianaiskandar on 4/5/16.
 */
public class SalesProfile extends RealmObject {

    @PrimaryKey
    private String kodeSales;
    private String namaSales;
    private String tanggal;

    public SalesProfile() {
    }

    public String getKodeSales() {
        return kodeSales;
    }

    public void setKodeSales(String kodeSales) {
        this.kodeSales = kodeSales;
    }

    public String getNamaSales() {
        return namaSales;
    }

    public void setNamaSales(String namaSales) {
        this.namaSales = namaSales;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }
}
