package main.sopianaiskandar.net.lubricantssales;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import main.sopianaiskandar.net.lubricantssales.adapter.CustomViewPager;
import main.sopianaiskandar.net.lubricantssales.fragment.FragmentEquipment;
import main.sopianaiskandar.net.lubricantssales.fragment.FragmentOwner;
import main.sopianaiskandar.net.lubricantssales.fragment.FragmentServices;
import main.sopianaiskandar.net.lubricantssales.fragment.FragmentValidation;
import main.sopianaiskandar.net.lubricantssales.helper.ImageUtils;
import main.sopianaiskandar.net.lubricantssales.model.ResultVisit;
import main.sopianaiskandar.net.lubricantssales.model.ResultVisitSales;

@SuppressLint("ValidFragment")
public class MainActivity extends Fragment {
    private TabLayout tabLayout;
    private CustomViewPager viewPager;
    public ViewPagerAdapter adapter;
    public HomeActivity home;

    public MainActivity(HomeActivity home) {
        this.home = home;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_main, container, false);
        viewPager = (CustomViewPager) rootView.findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        viewPager.setPagingEnabled(false);
        tabLayout = (TabLayout) rootView.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        LinearLayout tabStrip = ((LinearLayout) tabLayout.getChildAt(0));
        for (int i = 0; i < tabStrip.getChildCount(); i++) {
            tabStrip.getChildAt(i).setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return true;
                }
            });
        }
        return rootView;
    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new FragmentOwner(this, viewPager), "   Profile   ");
        adapter.addFragment(new FragmentEquipment(viewPager), "   Equipment   ");
        adapter.addFragment(new FragmentServices(viewPager), "   Service   ");
        adapter.addFragment(new FragmentValidation(this, viewPager), "   Validasi   ");
//        adapter.addFragment(new FragmentPreview(viewPager), "   Prev Identity   ");
//        adapter.addFragment(new FragmentPrevSegment(viewPager,home), "   Prev Produk   ");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    public void captureImage() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        HomeActivity activity = (HomeActivity) getActivity();

        if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                Toast toast = Toast.makeText(activity, "There was a problem saving the photo...", Toast.LENGTH_SHORT);
                toast.show();
            }
            if (photoFile != null) {
                Uri fileUri = Uri.fromFile(photoFile);
                activity.setCapturedImageURI(fileUri);
                activity.setCurrentPhotoPath(fileUri.getPath());
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        activity.getCapturedImageURI());
                startActivityForResult(takePictureIntent, FragmentValidation.CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
            }
        }
    }


    protected File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        HomeActivity activity = (HomeActivity) getActivity();
        activity.setCurrentPhotoPath("file:" + image.getAbsolutePath());
        return image;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == FragmentValidation.CAMERA_CAPTURE_IMAGE_REQUEST_CODE && resultCode == getActivity().RESULT_OK) {
            addPhotoToGallery();
            final HomeActivity activity = (HomeActivity) getActivity();
            Log.i("Path Poto " + FragmentValidation.photo, activity.getCurrentPhotoPath());
            final File imgFile = new File(activity.getCurrentPhotoPath());
            DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
            int height = displayMetrics.heightPixels;
            int width = displayMetrics.widthPixels;
            Glide.with(this)
                    .load(imgFile)
                    .asBitmap()
                    .fitCenter()
                    .override(width, height)
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                            try {
                                imgFile.delete();
                                File file = ImageUtils.saveImage(bitmap);
                                activity.setCurrentPhotoPath(file.getAbsolutePath());
                                ResultVisitSales item = HomeActivity.realm.where(ResultVisitSales.class).equalTo("id_visit", HomeActivity.dbLocal.getId_visit()).findFirst();
                                HomeActivity.realm.beginTransaction();
                                switch (FragmentValidation.photo) {
                                    case 1:
                                        setFullImageFromFilePath(activity.getCurrentPhotoPath(), FragmentValidation.img_1, 1);
                                        item.setPhoto1(activity.getCurrentPhotoPath());
                                        break;
                                    case 2:
                                        setFullImageFromFilePath(activity.getCurrentPhotoPath(), FragmentValidation.img_2, 2);
                                        item.setPhoto2(activity.getCurrentPhotoPath());
                                        break;
                                    case 3:
                                        setFullImageFromFilePath(activity.getCurrentPhotoPath(), FragmentValidation.img_3, 3);
                                        item.setPhoto3(activity.getCurrentPhotoPath());
                                        break;
                                    case 4:
                                        setFullImageFromFilePath(activity.getCurrentPhotoPath(), FragmentValidation.img_4, 4);
                                        item.setPhoto4(activity.getCurrentPhotoPath());
                                        break;
                                    case 5:
                                        setFullImageFromFilePath(activity.getCurrentPhotoPath(), FragmentValidation.img_5, 5);
                                        item.setPhoto5(activity.getCurrentPhotoPath());
                                        break;
                                    case 6:
                                        setFullImageFromFilePath(activity.getCurrentPhotoPath(), FragmentValidation.img_6, 6);
                                        item.setPhoto6(activity.getCurrentPhotoPath());
                                        break;
                                }
                                HomeActivity.realm.commitTransaction();
                            } catch (Exception e) {
                                HomeActivity.realm.cancelTransaction();
                            }

                        }
                    });


        } else {
            Toast.makeText(getActivity(), "Image Capture Failed", Toast.LENGTH_SHORT)
                    .show();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }


    protected void addPhotoToGallery() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        HomeActivity activity = (HomeActivity) getActivity();
        File f = new File(activity.getCurrentPhotoPath());
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.getActivity().sendBroadcast(mediaScanIntent);
    }

    private void setFullImageFromFilePath(String imagePath, ImageView imageView, int imgAt) {
        // Get the dimensions of the View
        int targetW = imageView.getWidth();
        int targetH = imageView.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imagePath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;
        Log.d("img path", imagePath);
        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        try {
        } catch (Exception e) {

        }
        Bitmap bitmap = BitmapFactory.decodeFile(imagePath, bmOptions);
        switch (imgAt) {
            case 1:
                FragmentValidation.setImgBtms(imagePath, FragmentValidation.img_1, 1);
                break;
            case 2:
                FragmentValidation.setImgBtms(imagePath, FragmentValidation.img_2, 2);
                break;
            case 3:
                FragmentValidation.setImgBtms(imagePath, FragmentValidation.img_3, 3);
                break;
            case 4:
                FragmentValidation.setImgBtms(imagePath, FragmentValidation.img_4, 4);
                break;
            case 5:
                FragmentValidation.setImgBtms(imagePath, FragmentValidation.img_5, 5);
                break;
            case 6:
                FragmentValidation.setImgBtms(imagePath, FragmentValidation.img_6, 6);
                break;
        }
    }


}
