package main.sopianaiskandar.net.lubricantssales.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import io.realm.RealmResults;
import main.sopianaiskandar.net.lubricantssales.AppController;
import main.sopianaiskandar.net.lubricantssales.Config;
import main.sopianaiskandar.net.lubricantssales.HomeActivity;
import main.sopianaiskandar.net.lubricantssales.LoginActivity;
import main.sopianaiskandar.net.lubricantssales.model.Cluster;

/**
 * Created by sopianaiskandar on 3/3/16.
 */
public class SyncronizeCluster {

    SessionManager manager;
    Activity act;

    public SyncronizeCluster(Activity cat) {
        this.act = cat;
        manager = new SessionManager(cat);
    }

    public void syncCluster(){
        String idRegion = manager.getRegion();
        delete();
        String url = Config.API_URL+"/getcluster?id_region="+idRegion;
        final ProgressDialog loading = ProgressDialog.show(this.act,"Downloading Cluster...","Please wait...",false,false);
        JsonArrayRequest req = new JsonArrayRequest(
               url,new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            Log.i("cluster", response.toString());
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject outlet = (JSONObject) response.get(i);
                                HomeActivity.realm.beginTransaction();
                                Cluster item = HomeActivity.realm.createObject(Cluster.class);
                                item.setCodeBranch(outlet.getString("code_branch"));
                                item.setNameBranch(outlet.getString("name_branch"));
                                HomeActivity.realm.commitTransaction();
                            }
                        }catch (Exception e){
                            HomeActivity.realm.cancelTransaction();}
                        loading.dismiss();
                    }
                },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loading.dismiss();
            }
        }
        );
        AppController.getInstance().addToRequestQueue(req);
    }

    public void delete(){
        try{
            RealmResults<Cluster> realmResults = HomeActivity.realm.where(Cluster.class).findAll();
            HomeActivity.realm.beginTransaction();
            realmResults.clear();
            HomeActivity.realm.commitTransaction();
        }catch (Exception e){
            HomeActivity.realm.cancelTransaction();
        }
    }
}
