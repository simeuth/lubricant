package main.sopianaiskandar.net.lubricantssales.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import main.sopianaiskandar.net.lubricantssales.R;
import main.sopianaiskandar.net.lubricantssales.model.Equipment;

/**
 * Created by simeuth on 26/02/2016.
 */
public class AdapterListEquipment extends BaseAdapter {
    private ArrayList<Equipment> _data;
    Context _context;

    public AdapterListEquipment(Context context, ArrayList<Equipment> data) {
        _data = data;
        _context = context;
    }

    @Override
    public int getCount() {

        return _data.size();
    }

    @Override
    public Object getItem(int position) {

        return _data.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        View v = convertView;
        LayoutInflater inflater = null;

        if (v == null) {
            inflater = (LayoutInflater) _context
                    .getSystemService(_context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.row_item_segmen, null);
            viewHolder = new ViewHolder();

            viewHolder.brand = (TextView) v.findViewById(R.id.nama_brand_segment);
            viewHolder.row = (LinearLayout) v.findViewById(R.id.row);
            viewHolder.checkList = (ImageView) v.findViewById(R.id.image_indicator);
            v.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) v.getTag();
        }

        if (_data.get(position).getIsSent()==0){
            viewHolder.checkList.setImageResource(R.drawable.red);
        }else {
            viewHolder.checkList.setImageResource(R.drawable.green);
        }


        viewHolder.brand.setText(_data.get(position).getNama());
        return v;
    }


    static class ViewHolder {
        TextView brand;
        ImageView checkList;
        LinearLayout row;
    }
}