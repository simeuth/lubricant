package main.sopianaiskandar.net.lubricantssales.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ResultVisitSales extends RealmObject {
    private int OliDari;
    private String a1;
    private String a2;
    private String a3;
    private String a4;
    private String address1;
    private String alamat;
    private String codeCustomer;
    private String codeSales;
    private String comment;
    private String contact;
    private String created_at;
    private String currentLat;
    private String currentLon;
    private String flag;
    private String gathering;
    private String idIndustri;
    private int idKab;
    private int idProv;
    private int idRegion;
    private int idRoute;
    private int idRute;
    private String idSegment;
    private int idTypeStore;
    private int idVisit;
    private int id_distributor;
    private int id_jenis_industri;
    private int id_kabupaten;
    private int id_perusahaan;
    private int id_provinsi;
    private int id_region;
    private int id_rute;
    private int id_sales;
    private int id_segment;

    @PrimaryKey
    private int id_visit;
    private String image;
    private int isBisnis;
    private int isClose;
    private int isCloseP;
    private int isMerchandise;
    private int isNoo;
    private int isNotFound;
    private int isSent;
    private int isUpdate;
    private int isVisit;
    private String is_visit;
    private int jumlahFlag;
    private int jumlahMerchandise;
    private int jumlahPoster;
    private int jumlahSpanduk;
    private String kab;
    private String keterangan;
    private String kodeCluster;
    private String kunjunganKe;
    private String lat;
    private String latCustomer;
    private String lon;
    private String longCustomer;
    private String merchandise;
    private int mesinEdc;
    private String namaCluster;
    private String namaPemilik;
    private String nameCustomer;
    private String no_hp;
    private int nooFromRoute;
    private String oil_clinic;
    private String p1;
    private String p2;
    private String pathImg1;
    private String pathImg2;
    private String pathImg3;
    private String pathImg4;
    private String pathImg5;
    private String pathImg6;
    private String pelatihan;
    private String perusahaan;
    private String phone;
    private String photo1;
    private String photo2;
    private String photo3;
    private String photo4;
    private String photo5;
    private String photo6;
    private String poster;
    private String prov;
    private String record_number;
    private String spanduk;

    public String getA1() {
        return this.a1;
    }

    public String getA2() {
        return this.a2;
    }

    public String getA3() {
        return this.a3;
    }

    public String getA4() {
        return this.a4;
    }

    public String getAddress1() {
        return this.address1;
    }

    public String getAlamat() {
        return this.alamat;
    }

    public String getCodeCustomer() {
        return this.codeCustomer;
    }

    public String getCodeSales() {
        return this.codeSales;
    }

    public String getComment() {
        return this.comment;
    }

    public String getContact() {
        return this.contact;
    }

    public String getCreated_at() {
        return this.created_at;
    }

    public String getCurrentLat() {
        return this.currentLat;
    }

    public String getCurrentLon() {
        return this.currentLon;
    }

    public String getFlag() {
        return this.flag;
    }

    public String getGathering() {
        return this.gathering;
    }

    public String getIdIndustri() {
        return this.idIndustri;
    }

    public int getIdKab() {
        return this.idKab;
    }

    public int getIdProv() {
        return this.idProv;
    }

    public int getIdRegion() {
        return this.idRegion;
    }

    public int getIdRoute() {
        return this.idRoute;
    }

    public int getIdRute() {
        return this.idRute;
    }

    public String getIdSegment() {
        return this.idSegment;
    }

    public int getIdTypeStore() {
        return this.idTypeStore;
    }

    public int getIdVisit() {
        return this.idVisit;
    }

    public int getId_distributor() {
        return this.id_distributor;
    }

    public int getId_jenis_industri() {
        return this.id_jenis_industri;
    }

    public int getId_kabupaten() {
        return this.id_kabupaten;
    }

    public int getId_perusahaan() {
        return this.id_perusahaan;
    }

    public int getId_provinsi() {
        return this.id_provinsi;
    }

    public int getId_region() {
        return this.id_region;
    }

    public int getId_rute() {
        return this.id_rute;
    }

    public int getId_sales() {
        return this.id_sales;
    }

    public int getId_segment() {
        return this.id_segment;
    }

    public int getId_visit() {
        return this.id_visit;
    }

    public String getImage() {
        return this.image;
    }

    public int getIsBisnis() {
        return this.isBisnis;
    }

    public int getIsClose() {
        return this.isClose;
    }

    public int getIsCloseP() {
        return this.isCloseP;
    }

    public int getIsMerchandise() {
        return this.isMerchandise;
    }

    public int getIsNoo() {
        return this.isNoo;
    }

    public int getIsNotFound() {
        return this.isNotFound;
    }

    public int getIsSent() {
        return this.isSent;
    }

    public int getIsUpdate() {
        return this.isUpdate;
    }

    public int getIsVisit() {
        return this.isVisit;
    }

    public String getIs_visit() {
        return this.is_visit;
    }

    public int getJumlahFlag() {
        return this.jumlahFlag;
    }

    public int getJumlahMerchandise() {
        return this.jumlahMerchandise;
    }

    public int getJumlahPoster() {
        return this.jumlahPoster;
    }

    public int getJumlahSpanduk() {
        return this.jumlahSpanduk;
    }

    public String getKab() {
        return this.kab;
    }

    public String getKeterangan() {
        return this.keterangan;
    }

    public String getKodeCluster() {
        return this.kodeCluster;
    }

    public String getKunjunganKe() {
        return this.kunjunganKe;
    }

    public String getLat() {
        return this.lat;
    }

    public String getLatCustomer() {
        return this.latCustomer;
    }

    public String getLon() {
        return this.lon;
    }

    public String getLongCustomer() {
        return this.longCustomer;
    }

    public String getMerchandise() {
        return this.merchandise;
    }

    public int getMesinEdc() {
        return this.mesinEdc;
    }

    public String getNamaCluster() {
        return this.namaCluster;
    }

    public String getNamaPemilik() {
        return this.namaPemilik;
    }

    public String getNameCustomer() {
        return this.nameCustomer;
    }

    public String getNo_hp() {
        return this.no_hp;
    }

    public int getNooFromRoute() {
        return this.nooFromRoute;
    }

    public String getOil_clinic() {
        return this.oil_clinic;
    }

    public int getOliDari() {
        return this.OliDari;
    }

    public String getP1() {
        return this.p1;
    }

    public String getP2() {
        return this.p2;
    }

    public String getPathImg1() {
        return this.pathImg1;
    }

    public String getPathImg2() {
        return this.pathImg2;
    }

    public String getPathImg3() {
        return this.pathImg3;
    }

    public String getPathImg4() {
        return this.pathImg4;
    }

    public String getPathImg5() {
        return this.pathImg5;
    }

    public String getPathImg6() {
        return this.pathImg6;
    }

    public String getPelatihan() {
        return this.pelatihan;
    }

    public String getPerusahaan() {
        return this.perusahaan;
    }

    public String getPhone() {
        return this.phone;
    }

    public String getPhoto1() {
        return this.photo1;
    }

    public String getPhoto2() {
        return this.photo2;
    }

    public String getPhoto3() {
        return this.photo3;
    }

    public String getPhoto4() {
        return this.photo4;
    }

    public String getPhoto5() {
        return this.photo5;
    }

    public String getPhoto6() {
        return this.photo6;
    }

    public String getPoster() {
        return this.poster;
    }

    public String getProv() {
        return this.prov;
    }

    public String getRecord_number() {
        return this.record_number;
    }

    public String getSpanduk() {
        return this.spanduk;
    }

    public void setA1(String paramString) {
        this.a1 = paramString;
    }

    public void setA2(String paramString) {
        this.a2 = paramString;
    }

    public void setA3(String paramString) {
        this.a3 = paramString;
    }

    public void setA4(String paramString) {
        this.a4 = paramString;
    }

    public void setAddress1(String paramString) {
        this.address1 = paramString;
    }

    public void setAlamat(String paramString) {
        this.alamat = paramString;
    }

    public void setCodeCustomer(String paramString) {
        this.codeCustomer = paramString;
    }

    public void setCodeSales(String paramString) {
        this.codeSales = paramString;
    }

    public void setComment(String paramString) {
        this.comment = paramString;
    }

    public void setContact(String paramString) {
        this.contact = paramString;
    }

    public void setCreated_at(String paramString) {
        this.created_at = paramString;
    }

    public void setCurrentLat(String paramString) {
        this.currentLat = paramString;
    }

    public void setCurrentLon(String paramString) {
        this.currentLon = paramString;
    }

    public void setFlag(String paramString) {
        this.flag = paramString;
    }

    public void setGathering(String paramString) {
        this.gathering = paramString;
    }

    public void setIdIndustri(String paramString) {
        this.idIndustri = paramString;
    }

    public void setIdKab(int paramInt) {
        this.idKab = paramInt;
    }

    public void setIdProv(int paramInt) {
        this.idProv = paramInt;
    }

    public void setIdRegion(int paramInt) {
        this.idRegion = paramInt;
    }

    public void setIdRoute(int paramInt) {
        this.idRoute = paramInt;
    }

    public void setIdRute(int paramInt) {
        this.idRute = paramInt;
    }

    public void setIdSegment(String paramString) {
        this.idSegment = paramString;
    }

    public void setIdTypeStore(int paramInt) {
        this.idTypeStore = paramInt;
    }

    public void setIdVisit(int paramInt) {
        this.idVisit = paramInt;
    }

    public void setId_distributor(int paramInt) {
        this.id_distributor = paramInt;
    }

    public void setId_jenis_industri(int paramInt) {
        this.id_jenis_industri = paramInt;
    }

    public void setId_kabupaten(int paramInt) {
        this.id_kabupaten = paramInt;
    }

    public void setId_perusahaan(int paramInt) {
        this.id_perusahaan = paramInt;
    }

    public void setId_provinsi(int paramInt) {
        this.id_provinsi = paramInt;
    }

    public void setId_region(int paramInt) {
        this.id_region = paramInt;
    }

    public void setId_rute(int paramInt) {
        this.id_rute = paramInt;
    }

    public void setId_sales(int paramInt) {
        this.id_sales = paramInt;
    }

    public void setId_segment(int paramInt) {
        this.id_segment = paramInt;
    }

    public void setId_visit(int paramInt) {
        this.id_visit = paramInt;
    }

    public void setImage(String paramString) {
        this.image = paramString;
    }

    public void setIsBisnis(int paramInt) {
        this.isBisnis = paramInt;
    }

    public void setIsClose(int paramInt) {
        this.isClose = paramInt;
    }

    public void setIsCloseP(int paramInt) {
        this.isCloseP = paramInt;
    }

    public void setIsMerchandise(int paramInt) {
        this.isMerchandise = paramInt;
    }

    public void setIsNoo(int paramInt) {
        this.isNoo = paramInt;
    }

    public void setIsNotFound(int paramInt) {
        this.isNotFound = paramInt;
    }

    public void setIsSent(int paramInt) {
        this.isSent = paramInt;
    }

    public void setIsUpdate(int paramInt) {
        this.isUpdate = paramInt;
    }

    public void setIsVisit(int paramInt) {
        this.isVisit = paramInt;
    }

    public void setIs_visit(String paramString) {
        this.is_visit = paramString;
    }

    public void setJumlahFlag(int paramInt) {
        this.jumlahFlag = paramInt;
    }

    public void setJumlahMerchandise(int paramInt) {
        this.jumlahMerchandise = paramInt;
    }

    public void setJumlahPoster(int paramInt) {
        this.jumlahPoster = paramInt;
    }

    public void setJumlahSpanduk(int paramInt) {
        this.jumlahSpanduk = paramInt;
    }

    public void setKab(String paramString) {
        this.kab = paramString;
    }

    public void setKeterangan(String paramString) {
        this.keterangan = paramString;
    }

    public void setKodeCluster(String paramString) {
        this.kodeCluster = paramString;
    }

    public void setKunjunganKe(String paramString) {
        this.kunjunganKe = paramString;
    }

    public void setLat(String paramString) {
        this.lat = paramString;
    }

    public void setLatCustomer(String paramString) {
        this.latCustomer = paramString;
    }

    public void setLon(String paramString) {
        this.lon = paramString;
    }

    public void setLongCustomer(String paramString) {
        this.longCustomer = paramString;
    }

    public void setMerchandise(String paramString) {
        this.merchandise = paramString;
    }

    public void setMesinEdc(int paramInt) {
        this.mesinEdc = paramInt;
    }

    public void setNamaCluster(String paramString) {
        this.namaCluster = paramString;
    }

    public void setNamaPemilik(String paramString) {
        this.namaPemilik = paramString;
    }

    public void setNameCustomer(String paramString) {
        this.nameCustomer = paramString;
    }

    public void setNo_hp(String paramString) {
        this.no_hp = paramString;
    }

    public void setNooFromRoute(int paramInt) {
        this.nooFromRoute = paramInt;
    }

    public void setOil_clinic(String paramString) {
        this.oil_clinic = paramString;
    }

    public void setOliDari(int paramInt) {
        this.OliDari = paramInt;
    }

    public void setP1(String paramString) {
        this.p1 = paramString;
    }

    public void setP2(String paramString) {
        this.p2 = paramString;
    }

    public void setPathImg1(String paramString) {
        this.pathImg1 = paramString;
    }

    public void setPathImg2(String paramString) {
        this.pathImg2 = paramString;
    }

    public void setPathImg3(String paramString) {
        this.pathImg3 = paramString;
    }

    public void setPathImg4(String paramString) {
        this.pathImg4 = paramString;
    }

    public void setPathImg5(String paramString) {
        this.pathImg5 = paramString;
    }

    public void setPathImg6(String paramString) {
        this.pathImg6 = paramString;
    }

    public void setPelatihan(String paramString) {
        this.pelatihan = paramString;
    }

    public void setPerusahaan(String paramString) {
        this.perusahaan = paramString;
    }

    public void setPhone(String paramString) {
        this.phone = paramString;
    }

    public void setPhoto1(String paramString) {
        this.photo1 = paramString;
    }

    public void setPhoto2(String paramString) {
        this.photo2 = paramString;
    }

    public void setPhoto3(String paramString) {
        this.photo3 = paramString;
    }

    public void setPhoto4(String paramString) {
        this.photo4 = paramString;
    }

    public void setPhoto5(String paramString) {
        this.photo5 = paramString;
    }

    public void setPhoto6(String paramString) {
        this.photo6 = paramString;
    }

    public void setPoster(String paramString) {
        this.poster = paramString;
    }

    public void setProv(String paramString) {
        this.prov = paramString;
    }

    public void setRecord_number(String paramString) {
        this.record_number = paramString;
    }

    public void setSpanduk(String paramString) {
        this.spanduk = paramString;
    }
}