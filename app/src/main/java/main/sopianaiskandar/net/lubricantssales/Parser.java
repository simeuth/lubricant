package main.sopianaiskandar.net.lubricantssales;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmObject;
import main.sopianaiskandar.net.lubricantssales.model.Equipment;
import main.sopianaiskandar.net.lubricantssales.model.Industry;
import main.sopianaiskandar.net.lubricantssales.model.Provinsi;
import main.sopianaiskandar.net.lubricantssales.model.Region;
import main.sopianaiskandar.net.lubricantssales.model.Segment;

/**
 * Created by simeuth on 23/07/2016.
 */
public class Parser {

    public static Gson getBuilder() {
        Gson gs = new GsonBuilder()
                .setExclusionStrategies(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {
                        return f.getDeclaringClass().equals(RealmObject.class);
                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> clazz) {
                        return false;
                    }
                })
                .create();
        return gs;
    }

    public static List<Industry> getListIndustry(String result) {
        List<Industry> arr = new ArrayList<>();
        arr = getBuilder().fromJson(result, new TypeToken<ArrayList<Industry>>() {
        }.getType());
        return arr;
    }


    public static List<Segment> getListSegments(String result) {
        List<Segment> arr = new ArrayList<>();
        arr = getBuilder().fromJson(result, new TypeToken<ArrayList<Segment>>() {
        }.getType());
        return arr;
    }

    public static ArrayList<Equipment> getEquipment(String result) {
        ArrayList<Equipment> arr = new ArrayList<>();
        arr = getBuilder().fromJson(result, new TypeToken<ArrayList<Equipment>>() {
        }.getType());
        return arr;
    }


    public static List<Region> getListKabupaten(String result) {
        List<Region> arr = new ArrayList<>();
        arr = getBuilder().fromJson(result, new TypeToken<ArrayList<Region>>() {
        }.getType());
        return arr;
    }

    public static List<Provinsi> getListPrivinsi(String result) {
        List<Provinsi> arr = new ArrayList<>();
        arr = getBuilder().fromJson(result, new TypeToken<ArrayList<Provinsi>>() {
        }.getType());
        return arr;
    }


}
