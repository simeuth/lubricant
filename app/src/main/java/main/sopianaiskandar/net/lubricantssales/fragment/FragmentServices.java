package main.sopianaiskandar.net.lubricantssales.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

import main.sopianaiskandar.net.lubricantssales.HomeActivity;
import main.sopianaiskandar.net.lubricantssales.R;
import main.sopianaiskandar.net.lubricantssales.model.ResultVisit;
import main.sopianaiskandar.net.lubricantssales.model.ResultVisitSales;

/**
 * Created by sopianaiskandar on 2/7/16.
 */
@SuppressLint("ValidFragment")
public class FragmentServices extends Fragment implements CompoundButton.OnCheckedChangeListener, View.OnClickListener {
    private CheckBox cek_pelatihan, cek_oil_clinic, cek_gathering;
    FloatingActionButton fab, fabSave;
    private ViewPager viewPager;
    private View forsnack;

    public FragmentServices(ViewPager viewPager) {
        this.viewPager = viewPager;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_material, container, false);
        cek_pelatihan = (CheckBox) rootview.findViewById(R.id.cek_pelatihan);
        cek_oil_clinic = (CheckBox) rootview.findViewById(R.id.cek_oil_clinic);
        cek_gathering = (CheckBox) rootview.findViewById(R.id.cek_gathering);

        cek_pelatihan.setOnCheckedChangeListener(this);
        cek_oil_clinic.setOnCheckedChangeListener(this);
        cek_gathering.setOnCheckedChangeListener(this);
        fab = (FloatingActionButton) rootview.findViewById(R.id.fab_material);
        fabSave = (FloatingActionButton) rootview.findViewById(R.id.button_save_material);
        fab.setOnClickListener(this);
        fabSave.setOnClickListener(this);

        try {
            if (HomeActivity.dbLocal.getGathering().equals("1")) {
                cek_gathering.setChecked(true);
            }

            if (HomeActivity.dbLocal.getOil_clinic().equals("1")) {
                cek_oil_clinic.setChecked(true);
            }

            if (HomeActivity.dbLocal.getPelatihan().equals("1")) {
                cek_pelatihan.setChecked(true);
            }
        } catch (Exception e) {

        }

        forsnack = rootview;
        return rootview;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        int id = buttonView.getId();
        switch (id) {

        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.fab_material:
                saveMaterial();
                break;
            case R.id.button_save_material:
                viewPager.setCurrentItem(1);
                break;
        }
    }

    public void saveMaterial() {
        save();
        viewPager.setCurrentItem(3);
    }

    public void save() {
        ResultVisitSales item = HomeActivity.realm.where(ResultVisitSales.class).equalTo("id_visit", HomeActivity.dbLocal.getId_visit()).findFirst();
        try {
            HomeActivity.realm.beginTransaction();
        } catch (Exception e) {
        }

        try {
            if (cek_gathering.isChecked()) {
                item.setGathering("1");
            } else {
                item.setGathering("0");
            }
            if (cek_oil_clinic.isChecked()) {
                item.setOil_clinic("1");
            } else {
                item.setOil_clinic("0");
            }
            if (cek_pelatihan.isChecked()) {
                item.setPelatihan("1");
            } else {
                item.setPelatihan("0");
            }

            HomeActivity.realm.commitTransaction();
//            Snackbar.make(forsnack, "Sukses Di Simpan", Snackbar.LENGTH_SHORT).show();
            HomeActivity.dbLocal = item;
        } catch (Exception e) {
            HomeActivity.realm.cancelTransaction();
        }

    }

}
