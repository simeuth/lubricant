package main.sopianaiskandar.net.lubricantssales;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import main.sopianaiskandar.net.lubricantssales.adapter.SessionManager;
import main.sopianaiskandar.net.lubricantssales.model.Industry;
import main.sopianaiskandar.net.lubricantssales.model.Provinsi;
import main.sopianaiskandar.net.lubricantssales.model.Region;
import main.sopianaiskandar.net.lubricantssales.model.Segment;

/**
 * Created by sopianaiskandar on 2/10/16.
 */
public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    Button btLogin;
    EditText username, password;
    private SessionManager session;
    public static Realm realm;
    private ArrayList<Industry> arrIndustries = new ArrayList();
    private ArrayList<Region> arrRegions = new ArrayList();
    private ArrayList<Segment> arrSegment = new ArrayList();
    private ArrayList<Provinsi> arrproProvinsis = new ArrayList();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        session = new SessionManager(getApplicationContext());

        setContentView(R.layout.activity_login);
        btLogin = (Button) findViewById(R.id.btnLogin);
        btLogin.setOnClickListener(this);
        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        RealmConfiguration config = new RealmConfiguration.Builder(this).build();
        Realm.setDefaultConfiguration(config);
        realm = Realm.getDefaultInstance();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (id == R.id.btnLogin) {
            authentication();
        }
    }

    public void authentication() {
        final String usText = this.username.getText().toString();
        final String pasText = this.password.getText().toString();

        final ProgressDialog loading = ProgressDialog.show(this, "Authentication...", "Please wait...", false, false);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        try {
                            JSONObject S = new JSONObject(s);
                            String status = S.getString("message");
                            if (status.equals("sukses")) {
                                try {
                                    JSONArray jsonObject = S.getJSONArray("identity");
                                    JSONObject objs = jsonObject.getJSONObject(0);
                                    session.setLogin(true, usText, "", objs.getString("nama"), objs.getString("nama"));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Log.d("respon", "Gagal Mengambil Data");
                                }

                                Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                                startActivity(intent);
                                finish();
                            } else {
                                Toast.makeText(getApplicationContext(), "username atau password salah", Toast.LENGTH_SHORT).show();
                            }
                            Toast.makeText(getApplicationContext(), S.getString("username atau password salah"), Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        loading.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        loading.dismiss();
                        Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();
                params.put("id_personal", usText);
                params.put("password", pasText);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }


    public void getDataJenisIndustri() {
        final ProgressDialog loading = ProgressDialog.show(this, "Memuat data master jenis industri", "Please wait...", false, false);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Config.JENIS_PERUSAHAAN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        loading.dismiss();
                        arrIndustries.clear();
                        arrIndustries.addAll(Parser.getListIndustry(s));
                        for (int i = 0; i < arrIndustries.size(); i++) {
                            try {
                                realm.beginTransaction();
                                Industry data = realm.createObject(Industry.class);
                                data.setId(arrIndustries.get(i).getId());
                                data.setNama(arrIndustries.get(i).getNama());
                                realm.commitTransaction();
                            } catch (Exception e) {
                                //
                            }
                        }
                        getDataSegment();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        loading.dismiss();
                        Toast.makeText(getApplicationContext(), "Gagal Mengambil Data", Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }


    public void getDataSegment() {
        final ProgressDialog loading = ProgressDialog.show(this, "Memuat data master segment", "Please wait...", false, false);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Config.GET_SEGMEN_ALL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        loading.dismiss();
                        arrSegment.clear();
                        arrSegment.addAll(Parser.getListSegments(s));
                        for (int i = 0; i < arrSegment.size(); i++) {
                            try {
                                realm.beginTransaction();
                                Segment data = realm.createObject(Segment.class);
                                data.setId(arrSegment.get(i).getId());
                                data.setNama(arrSegment.get(i).getNama());
                                realm.commitTransaction();
                            } catch (Exception e) {
                                //
                            }
                        }
                        getDataProvinsi();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        loading.dismiss();
                        Toast.makeText(getApplicationContext(), "Gagal Mengambil Data", Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }


    public void getDataProvinsi() {
        final ProgressDialog loading = ProgressDialog.show(this, "Memuat data master provinsi", "Please wait...", false, false);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Config.PROVINSI,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        arrproProvinsis.clear();
                        arrproProvinsis.addAll(Parser.getListPrivinsi(s));
                        for (int i = 0; i < arrproProvinsis.size(); i++) {
                            try {
                                realm.beginTransaction();
                                Provinsi data = realm.createObject(Provinsi.class);
                                data.setId(arrproProvinsis.get(i).getId());
                                data.setNama(arrproProvinsis.get(i).getNama());
                                realm.commitTransaction();
                            } catch (Exception e) {
                                //
                            }
                        }
                        loading.dismiss();
                        getDataKabupaten();
                    }
                }

                ,
                new Response.ErrorListener()

                {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        loading.dismiss();
                        Toast.makeText(getApplicationContext(), "Gagal Mengambil Data", Toast.LENGTH_LONG).show();
                    }
                }

        )

        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    public void getDataKabupaten() {
        final ProgressDialog loading = ProgressDialog.show(this, "Memuat data master kabupaten", "Please wait...", false, false);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Config.GET_KABUPATEN_ALL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        arrRegions.clear();
                        arrRegions.addAll(Parser.getListKabupaten(s));
                        for (int i = 0; i < arrRegions.size(); i++) {
                            try {
                                realm.beginTransaction();
                                Region data = realm.createObject(Region.class);
                                data.setId(arrRegions.get(i).getId());
                                data.setId_provinsi(arrRegions.get(i).getId_provinsi());
                                data.setName(arrRegions.get(i).getName());
                                realm.commitTransaction();
                            } catch (Exception e) {
                                //
                            }
                        }
                        loading.dismiss();
                        authentication();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        loading.dismiss();
                        Toast.makeText(getApplicationContext(), "Gagal Mengambil Data", Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }


}
