package main.sopianaiskandar.net.lubricantssales.adapter;

import android.app.Activity;

import java.text.SimpleDateFormat;
import java.util.Date;

import io.realm.RealmResults;
import main.sopianaiskandar.net.lubricantssales.HomeActivity;
import main.sopianaiskandar.net.lubricantssales.model.ResultVisit;

/**
 * Created by sopianaiskandar on 3/4/16.
 */
public class NooAdapter {
    Activity act;
    SessionManager manager;

    public NooAdapter(Activity act) {
        this.act = act;
        manager = new SessionManager(act);
    }

    public String generateKey(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyMMdd");
        Date date = new Date();
        String format = sdf.format(date);
        String code = manager.getCanvaserID()+format+getListNoo();
        return code;
    }

    public int getListNoo(){
        RealmResults<ResultVisit> result = HomeActivity.realm.where(ResultVisit.class).findAll();
        return result.size()+1;
    }
}
