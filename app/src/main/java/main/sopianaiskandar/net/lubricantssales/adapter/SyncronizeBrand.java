package main.sopianaiskandar.net.lubricantssales.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import io.realm.RealmResults;
import main.sopianaiskandar.net.lubricantssales.AppController;
import main.sopianaiskandar.net.lubricantssales.Config;
import main.sopianaiskandar.net.lubricantssales.HomeActivity;
import main.sopianaiskandar.net.lubricantssales.LoginActivity;
import main.sopianaiskandar.net.lubricantssales.model.Item;

/**
 * Created by sopianaiskandar on 3/3/16.
 */
public class SyncronizeBrand {

    Activity act;
    SessionManager manager;

    public SyncronizeBrand(Activity act) {
        this.act = act;
        this.manager = new SessionManager(this.act);
    }

    public void syncBrand(){
        delete();
        String url = Config.API_URL+"/item";
        final ProgressDialog loading = ProgressDialog.show(this.act,"Downloading Product list...","Please wait...",false,false);
        JsonArrayRequest req = new JsonArrayRequest(
                url,new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                try {
                    Log.i("brand",response.toString());
                    for (int i = 0; i < response.length(); i++) {
                        JSONObject outlet = (JSONObject) response.get(i);
                        HomeActivity.realm.beginTransaction();
                        Item item = HomeActivity.realm.createObject(Item.class);
                        item.setId(outlet.getInt("id"));
                        item.setBrand(outlet.getString("brand"));
                        item.setNamaProduct(outlet.getString("nama_product"));
                        HomeActivity.realm.commitTransaction();
                    }
                }catch (Exception e){
                    HomeActivity.realm.cancelTransaction();}
                loading.dismiss();
            }
        },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loading.dismiss();
            }
        }
        );
        AppController.getInstance().addToRequestQueue(req);

    }

    public void delete(){
        try{
            RealmResults<Item> realmResults = HomeActivity.realm.where(Item.class).findAll();
            HomeActivity.realm.beginTransaction();
            realmResults.clear();
            HomeActivity.realm.commitTransaction();
        }catch (Exception e){
            HomeActivity.realm.cancelTransaction();
        }
    }
}
