package main.sopianaiskandar.net.lubricantssales.callback;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public abstract interface OnParseJson<T> {
    public abstract List<T> parseJsonArray(JSONArray paramJSONArray);

    public abstract T parseJsonObject(JSONObject paramJSONObject);
}