package main.sopianaiskandar.net.lubricantssales.model;

/**
 * Created by sopianaiskandar on 3/26/16.
 */
public class ClosingCounter {

    private int idvisit;
    private int totalSegment;
    private int sentSegment;

    public ClosingCounter() {
    }

    public int getIdvisit() {
        return idvisit;
    }

    public void setIdvisit(int idvisit) {
        this.idvisit = idvisit;
    }

    public int getTotalSegment() {
        return totalSegment;
    }

    public void setTotalSegment(int totalSegment) {
        this.totalSegment = totalSegment;
    }

    public int getSentSegment() {
        return sentSegment;
    }

    public void setSentSegment(int sentSegment) {
        this.sentSegment = sentSegment;
    }
}
