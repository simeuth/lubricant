/**
 * @author egiadtya
 */
package main.sopianaiskandar.net.lubricantssales.callback;

public interface OnCallAPI {
	public void onAPIsuccess(String message);
	public void onAPIFailed(String errorMessage);
	public void executeAPI();
}
