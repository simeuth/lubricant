package main.sopianaiskandar.net.lubricantssales.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by simeuth on 23/07/2016.
 */
public class Segment extends RealmObject {
    @PrimaryKey
    private int id;
    private int id_jenis_industri;
    private String nama;

    public Segment() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_jenis_industri() {
        return id_jenis_industri;
    }

    public void setId_jenis_industri(int id_jenis_industri) {
        this.id_jenis_industri = id_jenis_industri;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }
}
