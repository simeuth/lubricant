package main.sopianaiskandar.net.lubricantssales;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Ack;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.HashMap;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import main.sopianaiskandar.net.lubricantssales.adapter.GPSTracker;
import main.sopianaiskandar.net.lubricantssales.adapter.SessionManager;
import main.sopianaiskandar.net.lubricantssales.fragment.FragmentConfirmation;
import main.sopianaiskandar.net.lubricantssales.fragment.FragmentDrawer;
import main.sopianaiskandar.net.lubricantssales.fragment.FragmentHistoryRoute;
import main.sopianaiskandar.net.lubricantssales.fragment.FragmentHome;
import main.sopianaiskandar.net.lubricantssales.fragment.FragmentListRoute;
import main.sopianaiskandar.net.lubricantssales.fragment.FragmentProfile;
import main.sopianaiskandar.net.lubricantssales.fragment.FragmentSales;
import main.sopianaiskandar.net.lubricantssales.model.Equipment;
import main.sopianaiskandar.net.lubricantssales.model.Provinsi;
import main.sopianaiskandar.net.lubricantssales.model.Region;
import main.sopianaiskandar.net.lubricantssales.model.ResultVisit;
import main.sopianaiskandar.net.lubricantssales.model.ResultVisitSales;
import main.sopianaiskandar.net.lubricantssales.model.Rute;

/**
 * Created by sopianaiskandar on 2/10/16.
 */
public class HomeActivity extends AppCompatActivity implements FragmentDrawer.FragmentDrawerListener {
    private Toolbar mToolbar;
    private FragmentDrawer drawerFragment;
    public static Realm realm;
    private SessionManager session;
    public static HashMap<String, Provinsi> mapProvinsi;
    public static HashMap<String, Region> mapRegion;
    public static HashMap<String, String> mapBrand;
    public static ResultVisitSales dbLocal;
    public static int statusnya;
    GPSTracker gps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        session = new SessionManager(this);
        String id_personal = session.getCanvaserID();
        mSocket.connect();
        mSocket.emit("new user", id_personal, new Ack() {
            @Override
            public void call(Object... args) {
                Log.d("callback", args.toString());
            }
        });
        mSocket.on("whisper", socketOn);
        gps = new GPSTracker(getApplicationContext());

        mapProvinsi = new HashMap<>();
        mapBrand = new HashMap<>();

        mapRegion = new HashMap<>();
        mapRegion.put("Region 1", new Region(1, "Region 1"));
        mapRegion.put("Region 2", new Region(2, "Region 2"));
        mapRegion.put("Region 3", new Region(3, "Region 3"));
        mapRegion.put("Region 4", new Region(4, "Region 4"));
        mapRegion.put("Region 5", new Region(5, "Region 5"));
        mapRegion.put("Region 6", new Region(6, "Region 6"));
        mapRegion.put("Region 7", new Region(7, "Region 7"));

        RealmConfiguration config = new RealmConfiguration.Builder(this).build();
        Realm.setDefaultConfiguration(config);
        realm = Realm.getDefaultInstance();

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        session = new SessionManager(getApplicationContext());
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        drawerFragment = (FragmentDrawer)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);
        drawerFragment.setDrawerListener(this);

        displayView(0);

        if (mCurrentPhotoPath != null) {
            savedInstanceState.putString(CAPTURED_PHOTO_PATH_KEY, mCurrentPhotoPath);
        }
        if (mCapturedImageURI != null) {
            savedInstanceState.putString(CAPTURED_PHOTO_URI_KEY, mCapturedImageURI.toString());
        }
//        super.onSaveInstanceState(savedInstanceState);


    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }

    public void displayView(int position) {
        Fragment fragment = null;
        String title = getString(R.string.app_name);
        switch (position) {
            case 0:
                //nampilin screen visit
                fragment = new FragmentHome();
                title = getString(R.string.title_home);
//                fragment = new MainActivity(this);
//                title = getString(R.string.title_friends);
                break;
            case 1:
                fragment = new FragmentListRoute(this);
                title = "Kunjungan Hari Ini";
                break;
            case 2:
                fragment = new FragmentHistoryRoute(this);
                title = "Histori Kunjungan";
                break;

            case 13:
                fragment = new MainActivity(this);
                title = getString(R.string.title_friends);
                break;
            case 3:
                fragment = new FragmentSales();
                title = "Closing";
                break;
            case 22:
                fragment = new FragmentListRoute();
                title = "Riwayat Kunjungan";
                break;
            case 5:
                logout();
                break;
            case 4:
                fragment = new FragmentProfile();
                title = "Profile Salesman";
                break;
            case 25:
                fragment = new FragmentConfirmation(this);
                break;
            default:
                break;
        }
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();
            getSupportActionBar().setTitle(title);
        }
    }

    public void logout() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Apakah Yakin Keluar Dari Aplikasi ?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        session.setLogin(false, "default", "-", "default", "-");
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private final static String CAPTURED_PHOTO_PATH_KEY = "mCurrentPhotoPath";
    private final static String CAPTURED_PHOTO_URI_KEY = "mCapturedImageURI";
    private String mCurrentPhotoPath = null;
    private Uri mCapturedImageURI = null;

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState.containsKey(CAPTURED_PHOTO_PATH_KEY)) {
            mCurrentPhotoPath = savedInstanceState.getString(CAPTURED_PHOTO_PATH_KEY);
        }
        if (savedInstanceState.containsKey(CAPTURED_PHOTO_URI_KEY)) {
            mCapturedImageURI = Uri.parse(savedInstanceState.getString(CAPTURED_PHOTO_URI_KEY));
        }
        super.onRestoreInstanceState(savedInstanceState);
    }

    public String getCurrentPhotoPath() {
        return mCurrentPhotoPath;
    }

    public void setCurrentPhotoPath(String mCurrentPhotoPath) {
        this.mCurrentPhotoPath = mCurrentPhotoPath;
    }

    public Uri getCapturedImageURI() {
        return mCapturedImageURI;
    }

    public void setCapturedImageURI(Uri mCapturedImageURI) {
        this.mCapturedImageURI = mCapturedImageURI;
    }

    public static String SocketUrl = "http://speedup.pertaminalubricants.com:8800/";
    private Socket mSocket;

    {
        try {
            mSocket = IO.socket(SocketUrl);
        } catch (URISyntaxException e) {
        }
    }

    private Emitter.Listener socketOn = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        JSONObject data = (JSONObject) args[0];
                        String to = data.getString("nick");
                        gps.getLocation();
                        if (gps.canGetLocation()) {
                            mSocket.emit("new message", "/w " + to + " " + gps.getLatitude() + "|" + gps.getLongitude());
                        } else {
                            mSocket.emit("new message", "/w " + to + " 0.0|0.0");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    private Boolean exit = false;

    @Override
    public void onBackPressed() {
        if (exit) {
            finish(); // finish activity
        } else {
            Toast.makeText(this, "Press Back again to Exit.",
                    Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);

        }

    }


    public void setIsUploaded() {
        ResultVisitSales localResultVisitSales = realm.where(ResultVisitSales.class).equalTo("id_visit", dbLocal.getId_visit()).findFirst();
        realm.beginTransaction();
        localResultVisitSales.setIsSent(1);
        realm.commitTransaction();
    }

    public boolean updateDataEquipment(Equipment paramEquipment, int paramInt) {
        try {
            realm.beginTransaction();
        } catch (Exception e) {

        }
        try {
            Equipment equipment = realm.where(Equipment.class).equalTo("id", paramEquipment.getId()).equalTo("id_visit_perusahaan", paramEquipment.getId_visit_perusahaan()).findFirst();
            equipment.setIsSent(paramInt);
            realm.commitTransaction();
            return true;
        } catch (Exception localException2) {
            realm.cancelTransaction();
            return false;
        }

    }


    public boolean setDataEquipment(Equipment paramEquipment, int paramInt) {
        try {
            realm.beginTransaction();
        } catch (Exception e) {

        }
        RealmResults<Equipment> equipments = realm.where(Equipment.class).equalTo("id", paramEquipment.getId()).equalTo("id_visit_perusahaan", paramEquipment.getId_perusahaan()).findAll();
        if (equipments.size() > 0) {
            return false;
        } else {
            Equipment localEquipment = realm.createObject(Equipment.class);
            localEquipment.setId_perusahaan(paramEquipment.getId_perusahaan());
            localEquipment.setId(paramEquipment.getId());
            localEquipment.setId_sales(paramEquipment.getId_sales());
            localEquipment.setIs_pertamina(paramEquipment.getIs_pertamina());
            localEquipment.setNama_pelumas(paramEquipment.getNama_pelumas());
            localEquipment.setVolume_kebutuhan(paramEquipment.getVolume_kebutuhan());
            localEquipment.setKapasitas_mesin(paramEquipment.getKapasitas_mesin());
            localEquipment.setNama(paramEquipment.getNama());
            localEquipment.setIsSent(paramInt);
            localEquipment.setId_visit_perusahaan(paramEquipment.getId_visit_perusahaan());
            realm.commitTransaction();
            return true;
        }

    }


}
