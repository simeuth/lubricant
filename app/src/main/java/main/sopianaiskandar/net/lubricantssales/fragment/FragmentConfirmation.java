package main.sopianaiskandar.net.lubricantssales.fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import main.sopianaiskandar.net.lubricantssales.Config;
import main.sopianaiskandar.net.lubricantssales.HomeActivity;
import main.sopianaiskandar.net.lubricantssales.LoginActivity;
import main.sopianaiskandar.net.lubricantssales.R;
import main.sopianaiskandar.net.lubricantssales.adapter.GPSTracker;
import main.sopianaiskandar.net.lubricantssales.adapter.SessionManager;
import main.sopianaiskandar.net.lubricantssales.controller.AsyncController;
import main.sopianaiskandar.net.lubricantssales.helper.ImageUtils;
import main.sopianaiskandar.net.lubricantssales.model.ResultVisit;
import main.sopianaiskandar.net.lubricantssales.model.ResultVisitSales;

/**
 * Created by sopianaiskandar on 3/5/16.
 */
@SuppressLint("ValidFragment")
public class FragmentConfirmation extends Fragment implements View.OnClickListener {

    Button takeLocation;
    TextView latitude, longitude;
    EditText keterangan;
    Bitmap btm;
    ImageView imges;
    View forsnack;
    FloatingActionButton fabSend, fabSave;
    HomeActivity home;
    private SessionManager sm;

    public FragmentConfirmation(HomeActivity home) {
        this.home = home;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_confirmation, container, false);
        sm = new SessionManager(getActivity());
        latitude = (TextView) rootview.findViewById(R.id.latitude_text);
        longitude = (TextView) rootview.findViewById(R.id.longitude_text);
        keterangan = (EditText) rootview.findViewById(R.id.edt_keterangan);
        fabSend = (FloatingActionButton) rootview.findViewById(R.id.fab_conf);
        fabSave = (FloatingActionButton) rootview.findViewById(R.id.fab_conf_save);
        takeLocation = (Button) rootview.findViewById(R.id.btn_location);
        imges = (ImageView) rootview.findViewById(R.id.view_image_conf);
        takeLocation.setOnClickListener(this);
        imges.setOnClickListener(this);
        fabSend.setOnClickListener(this);
        fabSave.setOnClickListener(this);
        forsnack = rootview;

        if (HomeActivity.dbLocal.getKeterangan() != null)
            this.keterangan.setText(HomeActivity.dbLocal.getKeterangan());
        if (HomeActivity.dbLocal.getImage() != null)
            setImgBtms(HomeActivity.dbLocal.getImage(), imges);
        if (HomeActivity.dbLocal.getLat() != null)
            this.latitude.setText("Latitude : " + HomeActivity.dbLocal.getLat());
        if (HomeActivity.dbLocal.getLon() != null)
            this.longitude.setText("Longitude : " + HomeActivity.dbLocal.getLon());

        return rootview;
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.fab_conf:
                try {
                    int i = HomeActivity.dbLocal.getId_visit();
                    ResultVisitSales localResultVisitSales = HomeActivity.realm.where(ResultVisitSales.class).equalTo("id_visit", Integer.valueOf(i)).findFirst();
                    if ((keterangan.getText().toString().trim().equals(""))
                            || (HomeActivity.dbLocal.getImage() == null)
                            || (HomeActivity.dbLocal.getLat().equals(""))
                            || (HomeActivity.dbLocal.getLon().equals(""))) {
                        Snackbar.make(forsnack, "Harap Isi seluruh data sebelum menyimpan", Snackbar.LENGTH_LONG).show();
                    } else {
                        uploadData(HomeActivity.dbLocal);
                    }
                } catch (Exception e) {
                    Snackbar.make(forsnack, "Harap Isi seluruh data sebelum menyimpan", Snackbar.LENGTH_LONG).show();

                }
                break;
            case R.id.view_image_conf:
                takePicture();
                break;
            case R.id.btn_location:
                takeLocation();
                break;
            case R.id.fab_conf_save:
                saveData(false);
                break;
        }
    }

    public void takePicture() {
        captureImage();
    }

    public void takeLocation() {
        GPSTracker gps = new GPSTracker(getActivity());
        if (gps.canGetLocation()) {
            double latitudes = gps.getLatitude();
            double longitudes = gps.getLongitude();
            longitude.setText("Longitude : " + longitudes);
            latitude.setText("Latitude : " + latitudes);
            try {
                HomeActivity.realm.beginTransaction();
                ResultVisitSales item = HomeActivity.realm.where(ResultVisitSales.class).equalTo("id_visit", HomeActivity.dbLocal.getId_visit()).findFirst();
                item.setLat(latitudes + "");
                item.setLon(longitudes + "");
                HomeActivity.realm.commitTransaction();
            } catch (Exception e) {
                HomeActivity.realm.cancelTransaction();
            }
        } else {
            gps.showSettingsAlert();
        }
    }

    public void captureImage() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        HomeActivity activity = (HomeActivity) getActivity();

        if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                Snackbar.make(forsnack, "There was a problem saving the photo...", Snackbar.LENGTH_SHORT).show();
            }
            if (photoFile != null) {
                Uri fileUri = Uri.fromFile(photoFile);
                activity.setCapturedImageURI(fileUri);
                activity.setCurrentPhotoPath(fileUri.getPath());
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        activity.getCapturedImageURI());
                startActivityForResult(takePictureIntent, FragmentValidation.CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
            }
        }
    }

    protected File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        HomeActivity activity = (HomeActivity) getActivity();
        activity.setCurrentPhotoPath("file:" + image.getAbsolutePath());
        return image;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == FragmentValidation.CAMERA_CAPTURE_IMAGE_REQUEST_CODE && resultCode == getActivity().RESULT_OK) {
            addPhotoToGallery();
            final HomeActivity activity = (HomeActivity) getActivity();
            Log.i("Path Poto " + FragmentValidation.photo, activity.getCurrentPhotoPath());
            final File imgFile = new File(activity.getCurrentPhotoPath());
            DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
            int height = displayMetrics.heightPixels;
            int width = displayMetrics.widthPixels;
            Glide.with(this)
                    .load(imgFile)
                    .asBitmap()
                    .fitCenter()
                    .override(width, height)
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                            try {
                                imgFile.delete();
                                File file = ImageUtils.saveImage(bitmap);
                                activity.setCurrentPhotoPath(file.getAbsolutePath());
                                HomeActivity.realm.beginTransaction();
                                ResultVisitSales item = HomeActivity.realm.where(ResultVisitSales.class).equalTo("id_visit", HomeActivity.dbLocal.getId_visit()).findFirst();
                                item.setImage(activity.getCurrentPhotoPath());
                                HomeActivity.realm.commitTransaction();
                                setImgBtms(file.getAbsolutePath(), imges);
                            } catch (Exception e) {
                                HomeActivity.realm.cancelTransaction();
                                Snackbar.make(forsnack, "Image Capture Failed", Snackbar.LENGTH_SHORT).show();
                            }

                        }
                    });


        } else {
            Snackbar.make(forsnack, "Image Capture Failed", Snackbar.LENGTH_SHORT).show();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    public void setImgBtms(String paramString, ImageView paramImageView) {
        BitmapFactory.Options localOptions = new BitmapFactory.Options();
        localOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(paramString, localOptions);
        int i = localOptions.outWidth;
        int j = localOptions.outHeight;
        int k = Math.min(i / 380, j / 200);
        localOptions.inJustDecodeBounds = false;
        localOptions.inPurgeable = true;
        localOptions.inSampleSize = k;
        paramImageView.setImageBitmap(BitmapFactory.decodeFile(paramString, localOptions));
        saveImage(paramString);
    }


    public void saveImage(String paramString) {
        int i = HomeActivity.dbLocal.getId_visit();
        ResultVisitSales localResultVisitSales = HomeActivity.realm.where(ResultVisitSales.class).equalTo("id_visit", Integer.valueOf(i)).findFirst();
        try {
            HomeActivity.realm.beginTransaction();
            localResultVisitSales.setImage(paramString);
            HomeActivity.realm.commitTransaction();
            HomeActivity.dbLocal = localResultVisitSales;
        } catch (Exception localException) {
            localException.printStackTrace();
            HomeActivity.realm.cancelTransaction();
        }
    }

    protected void addPhotoToGallery() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        HomeActivity activity = (HomeActivity) getActivity();
        File f = new File(activity.getCurrentPhotoPath());
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.getActivity().sendBroadcast(mediaScanIntent);
    }


    public void saveData(boolean isUpload) {
        try {
            int i = HomeActivity.dbLocal.getId_visit();
            ResultVisitSales localResultVisitSales = HomeActivity.realm.where(ResultVisitSales.class).equalTo("id_visit", Integer.valueOf(i)).findFirst();
            if ((keterangan.getText().toString().trim().equals(""))
                    || (HomeActivity.dbLocal.getImage() == null)
                    || (HomeActivity.dbLocal.getLat().equals(""))
                    || (HomeActivity.dbLocal.getLon().equals(""))) {
                Snackbar.make(forsnack, "Harap Isi seluruh data sebelum menyimpan", Snackbar.LENGTH_LONG).show();
            } else {
                HomeActivity.realm.beginTransaction();
                localResultVisitSales.setImage(HomeActivity.dbLocal.getImage());
                localResultVisitSales.setId_sales(Integer.valueOf(sm.getCanvaserID()).intValue());
                localResultVisitSales.setKeterangan(keterangan.getText().toString());
                if (isUpload) {
                    localResultVisitSales.setIsSent(1);
                }
                HomeActivity.realm.commitTransaction();
                HomeActivity.dbLocal = localResultVisitSales;
                if (isUpload) {
                    formNotification("Save data sukses, ");
                } else {
                    formNotification("Save data ke lokal sukses, ");
                }

                return;
            }
        } catch (Exception localException) {
            localException.printStackTrace();
            HomeActivity.realm.cancelTransaction();
        }
    }

    public void formNotification(String param) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(param + "kembali ke Home  ?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        home.displayView(0);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }


    public void uploadData(ResultVisitSales result) {
        final ProgressDialog loading = ProgressDialog.show(getActivity(), "Mengirim data...", "Please wait...", false, false);
        AsyncController controller = new AsyncController(getActivity(), result) {
            @Override
            public void onAPIsuccess(String message) {
                loading.dismiss();
                saveData(true);
                Log.e("JSON UPLOAD", message.toString());
            }

            @Override
            public void onAPIFailed(String errMessage) {
                loading.dismiss();
                if (errMessage.equals("null")) {
                    Snackbar.make(forsnack, "No internet connection", Snackbar.LENGTH_LONG).show();
                } else {
                    Snackbar.make(forsnack, "Upload gagal " + errMessage, Snackbar.LENGTH_LONG).show();
                }
            }

        };
        controller.setMethod(AsyncController.Method.UNVISIT);
        controller.executeAPI();
    }


}
