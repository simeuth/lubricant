package main.sopianaiskandar.net.lubricantssales.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import io.realm.RealmResults;
import main.sopianaiskandar.net.lubricantssales.Config;
import main.sopianaiskandar.net.lubricantssales.HomeActivity;
import main.sopianaiskandar.net.lubricantssales.LoginActivity;
import main.sopianaiskandar.net.lubricantssales.R;
import main.sopianaiskandar.net.lubricantssales.adapter.AdapterListRouteVisit;
import main.sopianaiskandar.net.lubricantssales.adapter.GPSTracker;
import main.sopianaiskandar.net.lubricantssales.adapter.NooAdapter;
import main.sopianaiskandar.net.lubricantssales.adapter.SessionManager;
import main.sopianaiskandar.net.lubricantssales.model.ResultVisit;
import main.sopianaiskandar.net.lubricantssales.model.ResultVisitSales;
import main.sopianaiskandar.net.lubricantssales.model.Rute;

@SuppressLint("ValidFragment")
public class FragmentListRoute extends Fragment implements AdapterView.OnItemClickListener, View.OnClickListener {
    private ListView lv;
    AdapterListRouteVisit adapter;
    EditText inputSearch;
    HomeActivity home;
    private String clicked;
    private int idVisit;
    public ResultVisitSales rute;
    GPSTracker gps;
    private ImageButton imgb;
    SessionManager sm;
    private ArrayList<ResultVisitSales> arrVisit = new ArrayList<>();

    public FragmentListRoute() {
    }

    @SuppressLint("ValidFragment")
    public FragmentListRoute(HomeActivity home) {
        this.home = home;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_list_route, container, false);
        lv = (ListView) rootview.findViewById(R.id.list_view);
        lv.setOnItemClickListener(this);
        inputSearch = (EditText) rootview.findViewById(R.id.inputSearch);
        imgb = (ImageButton) rootview.findViewById(R.id.fab_image_button);
        imgb.setVisibility(View.GONE);
        imgb.setOnClickListener(this);
        setAdapter();
        sm = new SessionManager(getActivity());
        return rootview;
    }

    public void setAdapter() {

        adapter = new AdapterListRouteVisit(getActivity(), arrVisit);
        lv.setAdapter(adapter);
        getArray();
        inputSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }


    public void getArray() {
        RealmResults<ResultVisitSales> visit = HomeActivity.realm.where(ResultVisitSales.class).findAll();
        for (ResultVisitSales c : visit) {
            if (c.getIs_visit().equals("0") && c.getIsSent() == 0) {
                ResultVisitSales item = new ResultVisitSales();
                item.setId_visit(c.getId_visit());
                item.setPerusahaan(c.getPerusahaan());
                item.setAlamat(c.getAlamat());
                item.setIs_visit(c.getIs_visit());
                item.setIsSent(c.getIsSent());
                arrVisit.add(item);
            }
        }

        adapter.notifyDataSetChanged();
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        gps = new GPSTracker(getActivity());
        Log.d("gps ", gps.canGetLocation() + "");
        if (gps.canGetLocation()) {
            rute = adapter.getData().get(position);
            idVisit = adapter.getData().get(position).getId_visit();
            openDialog();
        } else {
            showSettingsAlert();
        }
    }

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle("GPS is settings");
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                getActivity().startActivity(intent);
            }
        });
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }

    private Dialog d_kab;
    private Button updateData, berubahBisnis, tidakDitemukan, tutup, tutupPermanen, merchandiseData;

    public void openDialog() {
        d_kab = new Dialog(getActivity());
        d_kab.setTitle(" ");
        d_kab.setContentView(R.layout.dialog_first);
        updateData = (Button) d_kab.findViewById(R.id.btn_is_update);
        updateData.setOnClickListener(this);
        merchandiseData = (Button) d_kab.findViewById(R.id.btn_is_merchandise);
        merchandiseData.setOnClickListener(this);
        berubahBisnis = (Button) d_kab.findViewById(R.id.btn_is_bid);
        berubahBisnis.setOnClickListener(this);
        tidakDitemukan = (Button) d_kab.findViewById(R.id.btn_is_notfound);
        tidakDitemukan.setOnClickListener(this);
        tutup = (Button) d_kab.findViewById(R.id.btn_is_close);
        tutup.setOnClickListener(this);
        tutupPermanen = (Button) d_kab.findViewById(R.id.btn_is_close_p);
        tutupPermanen.setOnClickListener(this);
        d_kab.show();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.fab_image_button:
                String key = new NooAdapter(getActivity()).generateKey();
                Log.e("Eroronya  ===> ", key);
                break;
            case R.id.btn_is_update:
                this.title = "Update Data";
                d_kab.dismiss();
                setIsiRuteHome();
                home.displayView(13);
                break;

            case R.id.btn_is_merchandise:
                this.title = "Merchandise Data";
                d_kab.dismiss();
                setIsiRuteHome();
                home.displayView(28);
                break;

            case R.id.btn_is_notfound:
                this.title = "Not Found";
                d_kab.dismiss();
                setIsiRuteHome();
                HomeActivity.statusnya = 4;
                home.displayView(25);
                break;
            case R.id.btn_is_bid:
                this.title = "Berubah Bisnis";
                d_kab.dismiss();
                setIsiRuteHome();
                HomeActivity.statusnya = 3;
                home.displayView(25);
                break;
            case R.id.btn_is_close:
                this.title = "Tutup Sementara";
                d_kab.dismiss();
                setIsiRuteHome();
                HomeActivity.statusnya = 5;
                home.displayView(25);
                break;
            case R.id.btn_is_close_p:
                this.title = "Tutup Permanen";
                d_kab.dismiss();
                setIsiRuteHome();
                HomeActivity.statusnya = 6;
                home.displayView(25);
                break;
            case R.id.btn_save_is:
                execution = true;
                treatment();
                break;
            case R.id.btn_send_is:
                execution = false;
                treatment();
                break;
        }

    }


    String title = "";
    private Button save, send;
    private Dialog confirmDialog;


    public void confirmation() {
        Log.i("title", title);
        confirmDialog = new Dialog(getActivity());
        confirmDialog.setTitle(title);
        confirmDialog.setContentView(R.layout.dialog_confirmation);
        save = (Button) confirmDialog.findViewById(R.id.btn_save_is);
        send = (Button) confirmDialog.findViewById(R.id.btn_send_is);
        save.setOnClickListener(this);
        send.setOnClickListener(this);
        confirmDialog.show();
    }

    int choose;
    boolean execution;
    public static String UPLOAD_URL = Config.API_URL + "/upload-visit";
    ResultVisitSales item;

    public void treatment() {
        checkDataDiLocal();

        if (isAvaliable(clicked)) {
            //update
            item = create();
        } else {
            // create new
            item = update();
        }

        if (execution) {
            Log.i("simpan dilokal", "simpan di lokal dan kirim ke server nanti");
            updateIsVisit(item.getIdVisit());
            setAdapter();
        } else {
            Log.i("kirim ke server", "simpan di lokal dan kirim ke server");
            updateIsVisit(item.getIdVisit());
            setAdapter();
        }

        confirmDialog.dismiss();
    }

    public boolean isAvaliable(String index) {
        RealmResults<ResultVisitSales> result = HomeActivity.realm.where(ResultVisitSales.class).findAll();
        boolean avaliable = true;
        for (ResultVisitSales c : result) {
            Log.v("is avaliable ? " + c.getIdVisit(), "  --> " + idVisit + " ");
            if (idVisit == c.getIdVisit()) {
                avaliable = false;
                HomeActivity.dbLocal = c;
                break;
            }
        }
        return avaliable;
    }

    private void updateIsVisit(int idVisit) {
        ResultVisitSales item = HomeActivity.realm.where(ResultVisitSales.class).equalTo("id_visit", idVisit).findFirst();
        try {
            HomeActivity.realm.beginTransaction();
            item.setIs_visit("0");
            item.setId_sales(Integer.valueOf(sm.getCanvaserID()));
            Toast.makeText(getActivity(), "sukses", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            HomeActivity.realm.cancelTransaction();
            Log.e("eksepsi update", e.getMessage());
            Toast.makeText(getActivity(), "gagal", Toast.LENGTH_SHORT).show();
        }
    }

    private ResultVisitSales create() {
        try {
            HomeActivity.realm.beginTransaction();
            ResultVisitSales item = HomeActivity.realm.createObject(ResultVisitSales.class);
            item.setId_visit(idVisit);
            item.setIs_visit("0");
            item.setLat(gps.getLongitude() + "");
            item.setLon(gps.getLatitude() + "");
            HomeActivity.realm.commitTransaction();
            checkDataDiLocal();
            Toast.makeText(getActivity(), "sukses simpan", Toast.LENGTH_SHORT).show();
            return item;
        } catch (Exception e) {
            HomeActivity.realm.cancelTransaction();
            Log.e("eksepsi insert", e.getMessage());
            Toast.makeText(getActivity(), "gagal", Toast.LENGTH_SHORT).show();
            return null;
        }
    }

    private ResultVisitSales update() {
        ResultVisitSales item = HomeActivity.realm.where(ResultVisitSales.class).equalTo("id_visit", idVisit).findFirst();
        try {
            HomeActivity.realm.beginTransaction();
            item.setCurrentLon(this.gps.getLongitude() + "");
            item.setCurrentLat(this.gps.getLatitude() + "");
            HomeActivity.realm.commitTransaction();
            checkDataDiLocal();
            Toast.makeText(getActivity(), "sukses", Toast.LENGTH_SHORT).show();
            return item;
        } catch (Exception e) {
            HomeActivity.realm.cancelTransaction();
            Log.e("eksepsi update", e.getMessage());
            Toast.makeText(getActivity(), "gagal", Toast.LENGTH_SHORT).show();
            return null;
        }
    }

    private void checkDataDiLocal() {
        RealmResults<ResultVisitSales> result = HomeActivity.realm.where(ResultVisitSales.class).findAll();
        Log.d("total visit", result.size() + "");
        for (ResultVisitSales c : result) {
            Log.d("[ 'kode','id','nama']", c.getIdVisit() + ", cp " + c.getIsCloseP() + ", cs" + c.getIsClose() + ", nf " + c.getIsNotFound() + ", bc" + c.getIsBisnis());
        }

    }

    public void setIsiRuteHome() {
        ResultVisitSales item = HomeActivity.realm.where(ResultVisitSales.class).equalTo("id_visit", idVisit).findFirst();
        if (item != null) {
            HomeActivity.dbLocal = item;
        } else {
            HomeActivity.dbLocal = newObj();
        }
    }

    public ResultVisitSales newObj() {
        try {
            HomeActivity.realm.beginTransaction();
            ResultVisitSales item = HomeActivity.realm.createObject(ResultVisitSales.class);
            item.setId_visit(rute.getId_visit());
            item.setIs_visit("0");
            item.setId_rute(rute.getId_rute());
            item.setLat(rute.getLat());
            item.setLon(rute.getLon());
            item.setCreated_at(rute.getCreated_at());
            item.setRecord_number(rute.getRecord_number());
            item.setPerusahaan(rute.getPerusahaan());
            item.setAlamat(rute.getAlamat());
            item.setContact(rute.getContact());
            item.setNo_hp(rute.getNo_hp());
            item.setId_provinsi(rute.getId_provinsi());
            item.setId_kabupaten(rute.getId_kabupaten());
            item.setId_segment(rute.getId_segment());
            item.setId_distributor(rute.getId_distributor());
            item.setOil_clinic(rute.getOil_clinic());
            item.setPelatihan(rute.getPelatihan());
            item.setGathering(rute.getGathering());
            item.setId_region(rute.getId_region());
            item.setId_perusahaan(rute.getId_perusahaan());
            item.setId_jenis_industri(rute.getId_jenis_industri());
            item.setIsSent(0);
            HomeActivity.realm.commitTransaction();
            checkDataDiLocal();
            Toast.makeText(getActivity(), "sukses simpan", Toast.LENGTH_SHORT).show();
            return item;
        } catch (Exception e) {
            HomeActivity.realm.cancelTransaction();
            Log.e("eksepsi insert", e.getMessage());
            Toast.makeText(getActivity(), "gagal", Toast.LENGTH_SHORT).show();
            return null;
        }
    }


}