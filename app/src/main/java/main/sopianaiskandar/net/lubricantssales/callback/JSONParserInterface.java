package main.sopianaiskandar.net.lubricantssales.callback;

import org.json.JSONException;

public abstract interface JSONParserInterface
{
  public abstract boolean parse(String paramString)
    throws JSONException;
}