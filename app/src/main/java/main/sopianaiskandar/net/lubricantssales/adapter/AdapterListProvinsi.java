package main.sopianaiskandar.net.lubricantssales.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import main.sopianaiskandar.net.lubricantssales.R;
import main.sopianaiskandar.net.lubricantssales.model.Provinsi;
import main.sopianaiskandar.net.lubricantssales.model.ResultVisitSales;

public class AdapterListProvinsi extends BaseAdapter implements Filterable {
    Context _context;
    private ArrayList<Provinsi> _data;
    private ArrayList<Provinsi> _original;

    public AdapterListProvinsi(Context paramContext, ArrayList<Provinsi> paramArrayList) {
        this._data = paramArrayList;
        this._original = paramArrayList;
        this._context = paramContext;
    }

    public int getCount() {
        return this._data.size();
    }

    public ArrayList<Provinsi> getData() {
        return this._data;
    }


    public Object getItem(int paramInt) {
        return this._data.get(paramInt);
    }

    public long getItemId(int paramInt) {
        return paramInt;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        View v = convertView;
        LayoutInflater inflater = null;

        if (v == null) {
            inflater = (LayoutInflater) _context
                    .getSystemService(_context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.row_item_list, null);
            viewHolder = new ViewHolder();

            viewHolder.name = (TextView) v.findViewById(R.id.nama_brand_segment);
            v.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) v.getTag();
        }

        viewHolder.name.setText(_data.get(position).getNama());
        return v;
    }

    public Filter getFilter() {
        return new Filter() {
            protected FilterResults performFiltering(CharSequence paramCharSequence) {
                ArrayList<Provinsi> filteredResults = getFilteredResults(paramCharSequence);
                FilterResults results = new FilterResults();
                results.values = filteredResults;
                return results;
            }

            protected void publishResults(CharSequence paramCharSequence, FilterResults paramFilterResults) {
                _data = (ArrayList<Provinsi>) paramFilterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public ArrayList<Provinsi> getFilteredResults(CharSequence paramCharSequence) {
        ArrayList<Provinsi> localArrayList = new ArrayList();
        _data = _original;
        for (int i = 0; i < _data.size(); i++) {
            if (_data.get(i).getNama().toLowerCase().contains(paramCharSequence)) {
                localArrayList.add(this._data.get(i));
            }
        }
        return localArrayList;
    }

    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    static class ViewHolder {
        TextView name;
    }
}