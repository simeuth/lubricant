package main.sopianaiskandar.net.lubricantssales.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import main.sopianaiskandar.net.lubricantssales.R;
import main.sopianaiskandar.net.lubricantssales.model.ResultVisitSales;

public class AdapterListRouteVisit extends BaseAdapter implements Filterable {
    Context _context;
    private ArrayList<ResultVisitSales> _data;
    private ArrayList<ResultVisitSales> _original;

    public AdapterListRouteVisit(Context paramContext, ArrayList<ResultVisitSales> paramArrayList) {
        this._data = paramArrayList;
        this._original = paramArrayList;
        this._context = paramContext;
    }

    public int getCount() {
        return this._data.size();
    }

    public ArrayList<ResultVisitSales> getData() {
        return this._data;
    }


    public Object getItem(int paramInt) {
        return this._data.get(paramInt);
    }

    public long getItemId(int paramInt) {
        return paramInt;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        View v = convertView;
        LayoutInflater inflater = null;

        if (v == null) {
            inflater = (LayoutInflater) _context
                    .getSystemService(_context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.row_item_visit, null);
            viewHolder = new ViewHolder();

            viewHolder.image_indicator = (ImageView) v.findViewById(R.id.image_indicator);
            viewHolder.name = (TextView) v.findViewById(R.id.nama_brand_segment);
            v.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) v.getTag();
        }

        if (_data.get(position).getIsSent() == 0 && _data.get(position).getIs_visit().equals("0")) {
            viewHolder.image_indicator.setVisibility(View.GONE);
        } else if (_data.get(position).getIsSent() != 1) {
            viewHolder.image_indicator.setVisibility(View.VISIBLE);
            viewHolder.image_indicator.setImageResource(R.drawable.red);
        } else {
            viewHolder.image_indicator.setVisibility(View.VISIBLE);
            viewHolder.image_indicator.setImageResource(R.drawable.green);
        }

        viewHolder.name.setText(_data.get(position).getPerusahaan() + "\n" + _data.get(position).getAlamat());
        return v;
    }

    public Filter getFilter() {
        return new Filter() {
            protected Filter.FilterResults performFiltering(CharSequence paramCharSequence) {
                ArrayList<ResultVisitSales> filteredResults = getFilteredResults(paramCharSequence);
                FilterResults results = new FilterResults();
                results.values = filteredResults;
                return results;
            }

            protected void publishResults(CharSequence paramCharSequence, Filter.FilterResults paramFilterResults) {
                _data = (ArrayList<ResultVisitSales>) paramFilterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public ArrayList<ResultVisitSales> getFilteredResults(CharSequence paramCharSequence) {
        ArrayList<ResultVisitSales> localArrayList = new ArrayList();
        _data = _original;
        for (int i = 0; i < _data.size(); i++) {
            if (_data.get(i).getPerusahaan().toLowerCase().contains(paramCharSequence)) {
                localArrayList.add(this._data.get(i));
            }
        }
        return localArrayList;
    }

    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    static class ViewHolder {
        ImageView image_indicator;
        TextView name;
    }
}