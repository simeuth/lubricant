package main.sopianaiskandar.net.lubricantssales.model;

/**
 * Created by sopianaiskandar on 2/20/16.
 */
public class Kabupaten {
    private int id;
    private String nama;

    public Kabupaten(int id, String nama) {
        this.id = id;
        this.nama = nama;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }
}
