package main.sopianaiskandar.net.lubricantssales.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by sopianaiskandar on 3/2/16.
 */
public class ResultVisit extends RealmObject{
    @PrimaryKey
    private int idVisit;

    private int idRute;
    private int isVisit;
    private int idTypeStore;
    private String codeCustomer;
    private String nameCustomer;
    private String address1;
    private String phone;
    private String latCustomer;
    private String longCustomer;
    private String spanduk;
    private String flag;
    private String poster;
    private String merchandise;
    private String namaPemilik;
    private int idProv;
    private String prov;
    private int idKab;
    private String kab;
    private int idRegion;
    private String kodeCluster;
    private String namaCluster;
    private int isNoo;
    private int isUpdate;
    private int isNotFound;
    private int isClose;
    private int isCloseP;
    private int isBisnis;
    private String codeSales;
    private int idRoute;
    private String currentLat;
    private String currentLon;
    private int mesinEdc;
    private int OliDari;
    private int nooFromRoute;

    private String idIndustri;
    private String idSegment;

    private int jumlahSpanduk;
    private int jumlahMerchandise;
    private int jumlahFlag;
    private int jumlahPoster;

    private String pelatihan;
    private String oilClinic;
    private String gathering;

    private String pathImg1,pathImg2,pathImg3,pathImg4,pathImg5,pathImg6;
    private String comment;

    private String a1,a2,a3,a4,p1,p2;
    private String kunjunganKe;
    private int isMerchandise;
    private int isSent;



    public ResultVisit() {
    }

    public String getPelatihan() {
        return pelatihan;
    }

    public void setPelatihan(String pelatihan) {
        this.pelatihan = pelatihan;
    }

    public String getOilClinic() {
        return oilClinic;
    }

    public void setOilClinic(String oilClinic) {
        this.oilClinic = oilClinic;
    }

    public String getGathering() {
        return gathering;
    }

    public void setGathering(String gathering) {
        this.gathering = gathering;
    }

    public String getIdIndustri() {
        return idIndustri;
    }

    public void setIdIndustri(String idIndustri) {
        this.idIndustri = idIndustri;
    }

    public String getIdSegment() {
        return idSegment;
    }

    public void setIdSegment(String idSegment) {
        this.idSegment = idSegment;
    }

    public String getPathImg5() {
        return pathImg5;
    }

    public void setPathImg5(String pathImg5) {
        this.pathImg5 = pathImg5;
    }

    public String getPathImg6() {
        return pathImg6;
    }

    public void setPathImg6(String pathImg6) {
        this.pathImg6 = pathImg6;
    }

    public int getIdVisit() {
        return idVisit;
    }

    public void setIdVisit(int idVisit) {
        this.idVisit = idVisit;
    }

    public int getIdRute() {
        return idRute;
    }

    public void setIdRute(int idRute) {
        this.idRute = idRute;
    }

    public int getIsVisit() {
        return isVisit;
    }

    public void setIsVisit(int isVisit) {
        this.isVisit = isVisit;
    }

    public int getIdTypeStore() {
        return idTypeStore;
    }

    public void setIdTypeStore(int idTypeStore) {
        this.idTypeStore = idTypeStore;
    }

    public String getCodeCustomer() {
        return codeCustomer;
    }

    public void setCodeCustomer(String codeCustomer) {
        this.codeCustomer = codeCustomer;
    }

    public String getNameCustomer() {
        return nameCustomer;
    }

    public void setNameCustomer(String nameCustomer) {
        this.nameCustomer = nameCustomer;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLatCustomer() {
        return latCustomer;
    }

    public void setLatCustomer(String latCustomer) {
        this.latCustomer = latCustomer;
    }

    public String getLongCustomer() {
        return longCustomer;
    }

    public void setLongCustomer(String longCustomer) {
        this.longCustomer = longCustomer;
    }

    public String getSpanduk() {
        return spanduk;
    }

    public void setSpanduk(String spanduk) {
        this.spanduk = spanduk;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getMerchandise() {
        return merchandise;
    }

    public void setMerchandise(String merchandise) {
        this.merchandise = merchandise;
    }

    public String getNamaPemilik() {
        return namaPemilik;
    }

    public void setNamaPemilik(String namaPemilik) {
        this.namaPemilik = namaPemilik;
    }

    public int getIdProv() {
        return idProv;
    }

    public void setIdProv(int idProv) {
        this.idProv = idProv;
    }

    public String getProv() {
        return prov;
    }

    public void setProv(String prov) {
        this.prov = prov;
    }

    public int getIdKab() {
        return idKab;
    }

    public void setIdKab(int idKab) {
        this.idKab = idKab;
    }

    public String getKab() {
        return kab;
    }

    public void setKab(String kab) {
        this.kab = kab;
    }

    public int getIdRegion() {
        return idRegion;
    }

    public void setIdRegion(int idRegion) {
        this.idRegion = idRegion;
    }

    public String getKodeCluster() {
        return kodeCluster;
    }

    public void setKodeCluster(String kodeCluster) {
        this.kodeCluster = kodeCluster;
    }

    public String getNamaCluster() {
        return namaCluster;
    }

    public void setNamaCluster(String namaCluster) {
        this.namaCluster = namaCluster;
    }

    public int getIsNoo() {
        return isNoo;
    }

    public void setIsNoo(int isNoo) {
        this.isNoo = isNoo;
    }

    public int getIsUpdate() {
        return isUpdate;
    }

    public void setIsUpdate(int isUpdate) {
        this.isUpdate = isUpdate;
    }

    public int getIsNotFound() {
        return isNotFound;
    }

    public void setIsNotFound(int isNotFound) {
        this.isNotFound = isNotFound;
    }

    public int getIsClose() {
        return isClose;
    }

    public void setIsClose(int isClose) {
        this.isClose = isClose;
    }

    public int getIsCloseP() {
        return isCloseP;
    }

    public void setIsCloseP(int isCloseP) {
        this.isCloseP = isCloseP;
    }

    public int getIsBisnis() {
        return isBisnis;
    }

    public void setIsBisnis(int isBisnis) {
        this.isBisnis = isBisnis;
    }

    public String getCodeSales() {
        return codeSales;
    }

    public void setCodeSales(String codeSales) {
        this.codeSales = codeSales;
    }

    public int getIdRoute() {
        return idRoute;
    }

    public void setIdRoute(int idRoute) {
        this.idRoute = idRoute;
    }

    public String getCurrentLat() {
        return currentLat;
    }

    public void setCurrentLat(String currentLat) {
        this.currentLat = currentLat;
    }

    public String getCurrentLon() {
        return currentLon;
    }

    public void setCurrentLon(String currentLon) {
        this.currentLon = currentLon;
    }

    public int getMesinEdc() {
        return mesinEdc;
    }

    public void setMesinEdc(int mesinEdc) {
        this.mesinEdc = mesinEdc;
    }

    public int getOliDari() {
        return OliDari;
    }

    public void setOliDari(int oliDari) {
        OliDari = oliDari;
    }

    public int getNooFromRoute() {
        return nooFromRoute;
    }

    public void setNooFromRoute(int nooFromRoute) {
        this.nooFromRoute = nooFromRoute;
    }

    public int getJumlahSpanduk() {
        return jumlahSpanduk;
    }

    public void setJumlahSpanduk(int jumlahSpanduk) {
        this.jumlahSpanduk = jumlahSpanduk;
    }

    public int getJumlahMerchandise() {
        return jumlahMerchandise;
    }

    public void setJumlahMerchandise(int jumlahMerchandise) {
        this.jumlahMerchandise = jumlahMerchandise;
    }

    public int getJumlahFlag() {
        return jumlahFlag;
    }

    public void setJumlahFlag(int jumlahFlag) {
        this.jumlahFlag = jumlahFlag;
    }

    public int getJumlahPoster() {
        return jumlahPoster;
    }

    public void setJumlahPoster(int jumlahPoster) {
        this.jumlahPoster = jumlahPoster;
    }

    public String getPathImg1() {
        return pathImg1;
    }

    public void setPathImg1(String pathImg1) {
        this.pathImg1 = pathImg1;
    }

    public String getPathImg2() {
        return pathImg2;
    }

    public void setPathImg2(String pathImg2) {
        this.pathImg2 = pathImg2;
    }

    public String getPathImg3() {
        return pathImg3;
    }

    public void setPathImg3(String pathImg3) {
        this.pathImg3 = pathImg3;
    }

    public String getPathImg4() {
        return pathImg4;
    }

    public void setPathImg4(String pathImg4) {
        this.pathImg4 = pathImg4;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getA1() {
        return a1;
    }

    public void setA1(String a1) {
        this.a1 = a1;
    }

    public String getA2() {
        return a2;
    }

    public void setA2(String a2) {
        this.a2 = a2;
    }

    public String getA3() {
        return a3;
    }

    public void setA3(String a3) {
        this.a3 = a3;
    }

    public String getA4() {
        return a4;
    }

    public void setA4(String a4) {
        this.a4 = a4;
    }

    public String getP1() {
        return p1;
    }

    public void setP1(String p1) {
        this.p1 = p1;
    }

    public String getP2() {
        return p2;
    }

    public void setP2(String p2) {
        this.p2 = p2;
    }

    public String getKunjunganKe() {
        return kunjunganKe;
    }

    public void setKunjunganKe(String kunjunganKe) {
        this.kunjunganKe = kunjunganKe;
    }

    public int getIsMerchandise() {
        return isMerchandise;
    }

    public void setIsMerchandise(int isMerchandise) {
        this.isMerchandise = isMerchandise;
    }

    public int getIsSent() {
        return isSent;
    }

    public void setIsSent(int isSent) {
        this.isSent = isSent;
    }


}
