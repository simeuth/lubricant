package main.sopianaiskandar.net.lubricantssales.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import io.realm.RealmResults;
import main.sopianaiskandar.net.lubricantssales.Config;
import main.sopianaiskandar.net.lubricantssales.HomeActivity;
import main.sopianaiskandar.net.lubricantssales.Parser;
import main.sopianaiskandar.net.lubricantssales.R;
import main.sopianaiskandar.net.lubricantssales.adapter.AdapterListEquipment;
import main.sopianaiskandar.net.lubricantssales.adapter.AdapterListEquipmentPopup;
import main.sopianaiskandar.net.lubricantssales.adapter.SessionManager;
import main.sopianaiskandar.net.lubricantssales.model.Equipment;
import main.sopianaiskandar.net.lubricantssales.model.Item;
import main.sopianaiskandar.net.lubricantssales.model.MasterEquipment;
import main.sopianaiskandar.net.lubricantssales.model.ResultSegment;
import main.sopianaiskandar.net.lubricantssales.model.ResultVisitSales;

/**
 * Created by sopianaiskandar on 3/3/16.
 */
@SuppressLint("ValidFragment")
public class FragmentEquipment extends Fragment implements View.OnClickListener {

    private RealmResults<ResultSegment> mySegment;
    private Map<String, String> brand;
    private FloatingActionButton fabSave;
    private FloatingActionButton fabSend;
    private ViewPager forSwipe;
    private ListView l;
    private ArrayList<Equipment> arrSelected = new ArrayList<>();
    private ArrayList<Equipment> arrEquipment = new ArrayList<>();
    private AdapterListEquipment adapter;
    private Button btn_add_equipment;
    private SessionManager sm;
    View forsnack;

    public FragmentEquipment(ViewPager forSwipe) {
        this.forSwipe = forSwipe;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_segment, container, false);
        mySegment = HomeActivity.realm.where(ResultSegment.class).findAll();
        fabSave = (FloatingActionButton) rootView.findViewById(R.id.button_save_segmen);
        fabSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forSwipe.setCurrentItem(0);
            }
        });
        fabSend = (FloatingActionButton) rootView.findViewById(R.id.button_upload_segmen);
        fabSend.setOnClickListener(this);
        btn_add_equipment = (Button) rootView.findViewById(R.id.btn_add_equipment);
        btn_add_equipment.setOnClickListener(this);

        sm = new SessionManager(getActivity());

        // dari sini diganti segment adapter
        l = (ListView) rootView.findViewById(R.id.list_segment);
        adapter = new AdapterListEquipment(getActivity(), arrSelected);
        l.setAdapter(adapter);
        forsnack = rootView;
        try {
            arrSelected.clear();
            RealmResults<Equipment> results = HomeActivity.realm.where(Equipment.class).equalTo("id_visit_perusahaan", HomeActivity.dbLocal.getId_visit()).findAll();
            for (Equipment e : results) {
                Equipment equipment = new Equipment();
                equipment.setId(e.getId());
                equipment.setNama(e.getNama());
                equipment.setIsSent(e.getIsSent());
                arrSelected.add(equipment);
            }
            adapter.notifyDataSetChanged();

        } catch (Exception e) {

        }
        return rootView;
    }


    Dialog df;
    ListView list;
    AdapterListEquipmentPopup adapterPopup;

    public void dialogSegment(String message) {
        df = new Dialog(getActivity());
        df.setTitle(message);
        df.setContentView(R.layout.dialog_list_provinsi);
        list = (ListView) df.findViewById(R.id.list_prov);

        EditText edit = (EditText) df.findViewById(R.id.input_search_provinsi);
        edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adapterPopup.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        try {
            arrEquipment.clear();
            RealmResults<MasterEquipment> result = HomeActivity.realm.where(MasterEquipment.class).findAll();
            for (MasterEquipment c : result) {
                Equipment rs = new Equipment();
                rs.setId(c.getId());
                rs.setNama(c.getNama());
                arrEquipment.add(rs);
            }
            adapterPopup = new AdapterListEquipmentPopup(getActivity(), arrEquipment);
            list.setAdapter(adapterPopup);
        } catch (Exception e) {
            Log.e("error", e.getMessage());
        }


        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dialogFormEquipment("equipment", adapterPopup.getData().get(position));
                df.dismiss();
            }
        });

        df.show();
    }


    public void dialogFormEquipment(String message, final Equipment equipment) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setTitle(message);
        dialog.setContentView(R.layout.dialog_equipment_choose);

        RadioGroup radioGroup = (RadioGroup) dialog.findViewById(R.id.radioGroup);
        final EditText edt_nama_pelumas = (EditText) dialog.findViewById(R.id.edt_nama_pelumas);
        final EditText edt_volume_kebutuhan = (EditText) dialog.findViewById(R.id.edt_volume_kebutuhan);
        final EditText edt_kapasitas_mesin = (EditText) dialog.findViewById(R.id.edt_kapasitas_mesin);
        Button btn_save = (Button) dialog.findViewById(R.id.btn_save);

        equipment.setId_sales(Integer.valueOf(sm.getCanvaserID()).intValue());
        equipment.setId_visit_perusahaan(HomeActivity.dbLocal.getId_visit());
        equipment.setId_perusahaan(HomeActivity.dbLocal.getId_perusahaan());
        equipment.setIs_pertamina("1");
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup paramRadioGroup, int paramInt) {
                if (paramInt == R.id.radPertamina) {
                    equipment.setIs_pertamina("1");
                }
                if (paramInt == R.id.radKompetitor) {
                    equipment.setIs_pertamina("0");
                }
            }
        });

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ((edt_nama_pelumas.getText().toString().equals(""))
                        || (edt_volume_kebutuhan.getText().toString().equals(""))
                        || (edt_kapasitas_mesin.getText().toString().equals(""))) {
                    Toast.makeText(FragmentEquipment.this.getActivity(), "Harap isi form terlebih dahulu", Toast.LENGTH_LONG).show();
                } else {
                    equipment.setNama_pelumas(edt_nama_pelumas.getText().toString());
                    equipment.setVolume_kebutuhan(edt_volume_kebutuhan.getText().toString());
                    equipment.setKapasitas_mesin(edt_kapasitas_mesin.getText().toString());
                    savePenggunaanOli(equipment);
                    dialog.dismiss();
                }
            }
        });

        dialog.show();
    }


    public void savePenggunaanOli(final Equipment equipment) {
        final ProgressDialog loading = ProgressDialog.show(getActivity(), "Menyimpan...", "Please wait...", false, false);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.SAVE_PENGGUNAAN_OLI,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        equipment.setIsSent(1);
                        if (((HomeActivity) getActivity()).setDataEquipment(equipment, 1)) {
                            arrSelected.add(equipment);
                            adapter.notifyDataSetChanged();
//                            Toast.makeText(getActivity(), "Terkirim", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getActivity(), "Equipment ini telah ditambahkan sebelumnya", Toast.LENGTH_LONG).show();
                        }
                        loading.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        equipment.setIsSent(0);
                        if (((HomeActivity) getActivity()).setDataEquipment(equipment, 0)) {
                            arrSelected.add(equipment);
                            adapter.notifyDataSetChanged();
//                            Toast.makeText(getActivity(), "Tersimpan di lokal", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getActivity(), "Equipment ini telah ditambahkan sebelumnya", Toast.LENGTH_LONG).show();
                        }
                        loading.dismiss();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();
                params.put("id_perusahaan", equipment.getId_perusahaan() + "");
                params.put("id_mesin", equipment.getId() + "");
                params.put("id_sales", equipment.getId_sales() + "");
                params.put("is_pertamina", equipment.getIs_pertamina());
                params.put("nama_pelumas", equipment.getNama_pelumas());
                params.put("volume", equipment.getVolume_kebutuhan());
                params.put("kapasitas_mesin", equipment.getKapasitas_mesin());
                params.put("id_visit_perusahaan", equipment.getId_visit_perusahaan() + "");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }


    @Override
    public void onClick(View v) {
        if (v == btn_add_equipment) {
            dialogSegment("Select Equipment");
        }
        if (v == fabSend) {
            if (arrSelected.size() > 0) {
                forSwipe.setCurrentItem(2);
            } else {
                Snackbar.make(this.forsnack, "Anda belum mengisi surver equipment.", Snackbar.LENGTH_LONG).show();
            }
        }

    }
}
