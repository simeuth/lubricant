package main.sopianaiskandar.net.lubricantssales.model;

/**
 * Created by sopianaiskandar on 2/22/16.
 */
public class MainGPS {

    private String latitide;
    private String longitude;

    public MainGPS(String latitide, String longitude) {
        this.latitide = latitide;
        this.longitude = longitude;
    }

    public String getLatitide() {
        return latitide;
    }

    public void setLatitide(String latitide) {
        this.latitide = latitide;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
