package main.sopianaiskandar.net.lubricantssales;

/**
 * Created by simeuth on 23/07/2016.
 */
public class Config {
    //Set your base API url, so its easily to change later
    //Base Api
    public static String APP_NAME = "Lubricant";
    public static String HTTP = "http://";
    public static String DEVELOPMENT_URL = "industri.pertaminalubricants.com";
    public static String API_URL = HTTP + DEVELOPMENT_URL;
    public static String LOGIN = API_URL + "/login-sales";
    public static String JENIS_PERUSAHAAN = API_URL + "/jenis";
    public static String SEGMEN = API_URL + "/segment";
    public static String MESIN = API_URL + "/mesin";
    public static String MESIN_ALL = API_URL + "/mesinAll";
    public static String PROVINSI = API_URL + "/provinsi";
    public static String KABUPATEN = API_URL + "/kabupaten";
    public static String SAVE_PENGGUNAAN_OLI = API_URL + "/savePenggunaanOil";
    public static String SAVE_VISIT = API_URL + "/saveVisit";
    public static String GET_HISTORY_VISIT = API_URL + "/api-riwayat";
    public static String SAVE_UN_VISIT = API_URL + "/saveUnVisit";
    public static String CHANGE_PASSWORD = API_URL + "/api-change-password";
    public static String GET_KABUPATEN_ALL = API_URL + "/kabupaten-all";
    public static String GET_SEGMEN_ALL = API_URL + "/segmentAll";


    public static String getEquipment(String paramString) {
        return MESIN + "/" + paramString;
    }

    public static String getHJistori(String paramString) {
        return GET_HISTORY_VISIT + "?id_sales=" + paramString;
    }

    public static String getKabupaten(String paramString) {
        return KABUPATEN + "/" + paramString;
    }

    public static String getSegment(String paramString) {
        return SEGMEN + "/" + paramString;
    }
}
