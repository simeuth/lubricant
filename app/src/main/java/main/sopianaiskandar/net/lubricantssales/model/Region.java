package main.sopianaiskandar.net.lubricantssales.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by sopianaiskandar on 2/21/16.
 */
public class Region extends RealmObject {

    @PrimaryKey
    private int id;
    private int id_provinsi;
    private String name;
    private String region;

    public Region() {
    }

    public Region(int paramInt, String paramString) {
        this.id = paramInt;
        this.region = paramString;
    }

    public int getId() {
        return this.id;
    }

    public int getId_provinsi() {
        return this.id_provinsi;
    }

    public String getName() {
        return this.name;
    }

    public String getRegion() {
        return this.region;
    }

    public void setId(int paramInt) {
        this.id = paramInt;
    }

    public void setId_provinsi(int paramInt) {
        this.id_provinsi = paramInt;
    }

    public void setName(String paramString) {
        this.name = paramString;
    }

    public void setRegion(String paramString) {
        this.region = paramString;
    }
}