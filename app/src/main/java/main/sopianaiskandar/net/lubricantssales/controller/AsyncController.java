package main.sopianaiskandar.net.lubricantssales.controller;

import android.content.Context;

import org.json.JSONException;

import java.io.File;
import java.util.ArrayList;

import main.sopianaiskandar.net.lubricantssales.Config;
import main.sopianaiskandar.net.lubricantssales.model.ResultVisitSales;

/**
 * Created by simeuth on 10/06/2016.
 */
public class AsyncController extends BaseAPIController {
    private ArrayList<FileAttachment> fileAttachment;
    private Method method;
    private ResultVisitSales resultVisitSales;

    public AsyncController(Context paramContext, ResultVisitSales paramResultVisitSales) {
        super(paramContext);
        this.resultVisitSales = paramResultVisitSales;
    }

    @Override
    public void onAPIsuccess(String message) {

    }

    @Override
    public void executeAPI() {
        if (this.method == Method.UPLOAD_VISIT) {
            this.fileAttachment = new ArrayList();
            FileAttachment localFileAttachment2 = new FileAttachment();
            localFileAttachment2.setKey("photo1");
            localFileAttachment2.setFile(new File(resultVisitSales.getPhoto1()));
            FileAttachment localFileAttachment3 = new FileAttachment();
            localFileAttachment3.setKey("photo2");
            localFileAttachment3.setFile(new File(resultVisitSales.getPhoto2()));
            FileAttachment localFileAttachment4 = new FileAttachment();
            localFileAttachment4.setKey("photo3");
            localFileAttachment4.setFile(new File(resultVisitSales.getPhoto3()));
            FileAttachment localFileAttachment5 = new FileAttachment();
            localFileAttachment5.setKey("photo4");
            localFileAttachment5.setFile(new File(resultVisitSales.getPhoto4()));
            FileAttachment localFileAttachment6 = new FileAttachment();
            localFileAttachment6.setKey("photo5");
            localFileAttachment6.setFile(new File(resultVisitSales.getPhoto5()));
            FileAttachment localFileAttachment7 = new FileAttachment();
            localFileAttachment7.setKey("photo6");
            localFileAttachment7.setFile(new File(resultVisitSales.getPhoto6()));
            fileAttachment.add(localFileAttachment2);
            fileAttachment.add(localFileAttachment3);
            fileAttachment.add(localFileAttachment4);
            fileAttachment.add(localFileAttachment5);
            fileAttachment.add(localFileAttachment6);
            fileAttachment.add(localFileAttachment7);
            addParameter("id_visit", resultVisitSales.getId_visit() + "");
            addParameter("contact_person", resultVisitSales.getContact() + "");
            addParameter("sales_lat", resultVisitSales.getLat() + "");
            addParameter("sales_lon", resultVisitSales.getLon() + "");
            addParameter("id_sales", resultVisitSales.getId_sales() + "");
            addParameter("is_visit", "1");
            addParameter("phone", resultVisitSales.getNo_hp() + "");
            addParameter("id_perusahaan", resultVisitSales.getId_perusahaan() + "");
            addParameter("nama_perusahaan", resultVisitSales.getPerusahaan() + "");
            addParameter("id_kabupaten", resultVisitSales.getId_kabupaten() + "");
            addParameter("id_provinsi", resultVisitSales.getId_provinsi() + "");
            addParameter("id_segment", resultVisitSales.getId_segment() + "");
            addParameter("oil_clinic", resultVisitSales.getOil_clinic() + "");
            addParameter("pelatihan", resultVisitSales.getPelatihan() + "");
            addParameter("gathering", resultVisitSales.getGathering() + "");
            POST_WITH_MANY_IMAGE(Config.SAVE_VISIT, fileAttachment);
        }
        if (this.method == Method.UNVISIT) {
            this.fileAttachment = new ArrayList();
            FileAttachment localFileAttachment1 = new FileAttachment();
            localFileAttachment1.setKey("photo1");
            localFileAttachment1.setFile(new File(resultVisitSales.getImage()));
            this.fileAttachment.add(localFileAttachment1);
            addParameter("id_visit", resultVisitSales.getId_visit() + "");
            addParameter("id_sales", resultVisitSales.getId_sales() + "");
            addParameter("sales_lat", resultVisitSales.getLat() + "");
            addParameter("sales_lon", resultVisitSales.getLon() + "");
            addParameter("is_visit", "0");
            addParameter("keterangan", resultVisitSales.getKeterangan() + "");
            POST_WITH_MANY_IMAGE(Config.SAVE_UN_VISIT, fileAttachment);
        }

    }

    public void setMethod(Method method) {
        this.method = method;
    }

    @Override
    public boolean parse(String json) throws JSONException {
        return false;
    }

    public enum Method {
        UNVISIT,
        UPLOAD_VISIT
    }
}
