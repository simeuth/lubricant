package main.sopianaiskandar.net.lubricantssales.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import main.sopianaiskandar.net.lubricantssales.R;
import main.sopianaiskandar.net.lubricantssales.model.Region;
import main.sopianaiskandar.net.lubricantssales.model.ResultVisitSales;

public class AdapterListKabupaten extends BaseAdapter implements Filterable {
    Context _context;
    private ArrayList<Region> _data;
    private ArrayList<Region> _original;

    public AdapterListKabupaten(Context paramContext, ArrayList<Region> paramArrayList) {
        this._data = paramArrayList;
        this._original = paramArrayList;
        this._context = paramContext;
    }

    public int getCount() {
        return this._data.size();
    }

    public ArrayList<Region> getData() {
        return this._data;
    }


    public Object getItem(int paramInt) {
        return this._data.get(paramInt);
    }

    public long getItemId(int paramInt) {
        return paramInt;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        View v = convertView;
        LayoutInflater inflater = null;

        if (v == null) {
            inflater = (LayoutInflater) _context
                    .getSystemService(_context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.row_item_list, null);
            viewHolder = new ViewHolder();

            viewHolder.name = (TextView) v.findViewById(R.id.nama_brand_segment);
            v.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) v.getTag();
        }


        viewHolder.name.setText(_data.get(position).getName());
        return v;
    }

    public Filter getFilter() {
        return new Filter() {
            protected FilterResults performFiltering(CharSequence paramCharSequence) {
                ArrayList<Region> filteredResults = getFilteredResults(paramCharSequence);
                FilterResults results = new FilterResults();
                results.values = filteredResults;
                return results;
            }

            protected void publishResults(CharSequence paramCharSequence, FilterResults paramFilterResults) {
                _data = (ArrayList<Region>) paramFilterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public ArrayList<Region> getFilteredResults(CharSequence paramCharSequence) {
        ArrayList<Region> localArrayList = new ArrayList();
        _data = _original;
        for (int i = 0; i < _data.size(); i++) {
            if (_data.get(i).getName().toLowerCase().contains(paramCharSequence)) {
                localArrayList.add(this._data.get(i));
            }
        }
        return localArrayList;
    }

    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    static class ViewHolder {
        TextView name;
    }
}