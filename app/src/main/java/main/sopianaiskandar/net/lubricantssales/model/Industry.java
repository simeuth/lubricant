package main.sopianaiskandar.net.lubricantssales.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by simeuth on 23/07/2016.
 */
public class Industry extends RealmObject {
    @PrimaryKey
    private int id;
    private String nama;


    public Industry() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

}
