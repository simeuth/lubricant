package main.sopianaiskandar.net.lubricantssales.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Provinsi extends RealmObject
{
    private String Nama;

    @PrimaryKey
    private int id;
    private int region;

    public int getId()
    {
        return this.id;
    }

    public String getNama()
    {
        return this.Nama;
    }

    public int getRegion()
    {
        return this.region;
    }

    public void setId(int paramInt)
    {
        this.id = paramInt;
    }

    public void setNama(String paramString)
    {
        this.Nama = paramString;
    }

    public void setRegion(int paramInt)
    {
        this.region = paramInt;
    }
}