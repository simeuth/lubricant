package main.sopianaiskandar.net.lubricantssales.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by sopianaiskandar on 2/21/16.
 */
public class TipeOutlet extends RealmObject {

    @PrimaryKey
    private int id;
    private String nama;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }


    public TipeOutlet() {
    }

    public TipeOutlet(int id, String nama) {
        this.id = id;
        this.nama = nama;
    }
}
