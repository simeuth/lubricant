package main.sopianaiskandar.net.lubricantssales.fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

import io.realm.RealmObject;
import io.realm.RealmResults;
import main.sopianaiskandar.net.lubricantssales.Config;
import main.sopianaiskandar.net.lubricantssales.HomeActivity;
import main.sopianaiskandar.net.lubricantssales.MainActivity;
import main.sopianaiskandar.net.lubricantssales.R;
import main.sopianaiskandar.net.lubricantssales.adapter.GPSTracker;
import main.sopianaiskandar.net.lubricantssales.controller.AsyncController;
import main.sopianaiskandar.net.lubricantssales.model.Equipment;
import main.sopianaiskandar.net.lubricantssales.model.ResultVisit;
import main.sopianaiskandar.net.lubricantssales.model.ResultVisitSales;

/**
 * Created by sopianaiskandar on 2/7/16.
 */
@SuppressLint("ValidFragment")
public class FragmentValidation extends Fragment implements View.OnClickListener {
    public static ImageView img_1, img_2, img_3, img_4, img_5, img_6;
    public static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    public FloatingActionButton  fabsave;
    private MainActivity maink;
    Button takeLocation;
    TextView latitude, longitude;
    private ViewPager viewPager;
    private View forsnack;
    Button btn_save_to_locale, btn_upload;
    private ArrayList<Equipment> arrEquipment = new ArrayList<>();
    private int iterator = 0;

    @SuppressLint("ValidFragment")
    public FragmentValidation(MainActivity main, ViewPager viewPager) {
        this.viewPager = viewPager;
        this.maink = main;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_validation, container, false);
        latitude = (TextView) rootview.findViewById(R.id.latitude_text_v);
        longitude = (TextView) rootview.findViewById(R.id.longitude_text_v);
        takeLocation = (Button) rootview.findViewById(R.id.btn_location_v);
        btn_save_to_locale = (Button) rootview.findViewById(R.id.btn_save_to_locale);
        btn_upload = (Button) rootview.findViewById(R.id.btn_upload);
        takeLocation.setOnClickListener(this);
        img_1 = (ImageView) rootview.findViewById(R.id.view_image_1);
        img_2 = (ImageView) rootview.findViewById(R.id.view_image_2);
        img_3 = (ImageView) rootview.findViewById(R.id.view_image_3);
        img_4 = (ImageView) rootview.findViewById(R.id.view_image_4);
        img_5 = (ImageView) rootview.findViewById(R.id.view_image_5);
        img_6 = (ImageView) rootview.findViewById(R.id.view_image_6);
        img_1.setOnClickListener(this);
        img_2.setOnClickListener(this);
        img_3.setOnClickListener(this);
        img_4.setOnClickListener(this);
        img_5.setOnClickListener(this);
        img_6.setOnClickListener(this);
        btn_save_to_locale.setOnClickListener(this);
        btn_upload.setOnClickListener(this);

        try {
            HomeActivity.realm.beginTransaction();
            if (HomeActivity.dbLocal.getPhoto1() != null) {
                setImgBtm(HomeActivity.dbLocal.getPhoto1(), img_1, 1);
            }
            if (HomeActivity.dbLocal.getPhoto2() != null) {
                setImgBtm(HomeActivity.dbLocal.getPhoto2(), img_2, 2);
            }
            if (HomeActivity.dbLocal.getPhoto3() != null) {
                setImgBtm(HomeActivity.dbLocal.getPhoto3(), img_3, 3);
            }
            if (HomeActivity.dbLocal.getPhoto4() != null) {
                setImgBtm(HomeActivity.dbLocal.getPhoto4(), img_4, 4);
            }
            if (HomeActivity.dbLocal.getPhoto5() != null) {
                setImgBtm(HomeActivity.dbLocal.getPhoto5(), img_5, 5);
            }
            if (HomeActivity.dbLocal.getPhoto6() != null) {
                setImgBtm(HomeActivity.dbLocal.getPhoto6(), img_6, 6);
            }
        } catch (Exception e) {
            HomeActivity.realm.cancelTransaction();
        }


        getEquipment();

        fabsave = (FloatingActionButton) rootview.findViewById(R.id.fab_conf_save_v);
        fabsave.setOnClickListener(this);

        longitude.setText("Longitude : " + HomeActivity.dbLocal.getLat());
        latitude.setText("Latitude : " + HomeActivity.dbLocal.getLon());

        forsnack = rootview;
        return rootview;
    }

    public void getEquipment() {
        try {
            arrEquipment.clear();
            RealmResults<Equipment> results = HomeActivity.realm.where(Equipment.class).equalTo("id_visit_perusahaan", HomeActivity.dbLocal.getId_visit()).findAll();
            for (Equipment e : results) {
                if (e.getIsSent() == 0) {
                    Equipment equipment = new Equipment();
                    equipment.setId_perusahaan(e.getId_perusahaan());
                    equipment.setId(e.getId());
                    equipment.setId_sales(e.getId_sales());
                    equipment.setIs_pertamina(e.getIs_pertamina());
                    equipment.setNama_pelumas(e.getNama_pelumas());
                    equipment.setVolume_kebutuhan(e.getVolume_kebutuhan());
                    equipment.setKapasitas_mesin(e.getKapasitas_mesin());
                    equipment.setId_visit_perusahaan(e.getId_visit_perusahaan());
                    arrEquipment.add(equipment);
                }
            }
        } catch (Exception e) {

        }
    }


    public static int photo = 1;

    @Override
    public void onClick(View v) {
        int idClicked = v.getId();
        switch (idClicked) {
            case R.id.view_image_1:
                photo = 1;
                maink.captureImage();
                break;
            case R.id.view_image_2:
                photo = 2;
                maink.captureImage();
                break;
            case R.id.view_image_3:
                photo = 3;
                maink.captureImage();
                break;
            case R.id.view_image_4:
                photo = 4;
                maink.captureImage();
                break;
            case R.id.view_image_5:
                photo = 5;
                maink.captureImage();
                break;
            case R.id.view_image_6:
                photo = 6;
                maink.captureImage();
                break;
            case R.id.btn_location_v:
                takeLocation();
                break;
            case R.id.btn_upload:
                getEquipment();
                try {
                    if ((HomeActivity.dbLocal.getLat().equals("")) ||
                            (HomeActivity.dbLocal.getLon().equals("")) ||
                            (HomeActivity.dbLocal.getPhoto1().equals("")) ||
                            (HomeActivity.dbLocal.getPhoto2().equals("")) ||
                            (HomeActivity.dbLocal.getPhoto3().equals("")) ||
                            (HomeActivity.dbLocal.getPhoto4().equals("")) ||
                            (HomeActivity.dbLocal.getPhoto5().equals("")) ||
                            (HomeActivity.dbLocal.getPhoto6().equals(""))) {
                        Snackbar.make(this.forsnack, "Anda belum mengisi seluruh data survey. Ambil lokasi dan foto survey terlebih dahulu.", Snackbar.LENGTH_LONG).show();
                    } else {
                        iterator = 0;
                        if (arrEquipment.size() > 0) {
                            savePenggunaanOli(arrEquipment.get(iterator), iterator, arrEquipment.size());
                        } else {
                            uploadData(HomeActivity.dbLocal);
                        }
                    }
                } catch (Exception e) {
                    Snackbar.make(this.forsnack, "Anda belum mengisi seluruh data survey. Ambil lokasi dan foto survey terlebih dahulu.", Snackbar.LENGTH_LONG).show();
                }
                break;
            case R.id.btn_save_to_locale:
                saveToLocale(false);
                break;
            case R.id.fab_conf_save_v:
                viewPager.setCurrentItem(2);
                break;
        }
    }


    private void setImgBtm(String imagePath, ImageView imageView, int imgAt) {
        int targetW = 380;
        int targetH = 200;
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;

        BitmapFactory.decodeFile(imagePath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inPurgeable = true;
        bmOptions.inSampleSize = scaleFactor;

        Bitmap bitmap = BitmapFactory.decodeFile(imagePath, bmOptions);
        switch (imgAt) {
            case 1:
                imageView.setImageBitmap(bitmap);
                break;
            case 2:
                imageView.setImageBitmap(bitmap);
                break;
            case 3:
                imageView.setImageBitmap(bitmap);
                break;
            case 4:
                imageView.setImageBitmap(bitmap);
                break;
            case 5:
                imageView.setImageBitmap(bitmap);
                break;
            case 6:
                imageView.setImageBitmap(bitmap);
                break;
        }
    }

    public static void setImgBtms(String imagePath, ImageView imageView, int imgAt) {
        int targetW = 380;
        int targetH = 200;
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;

        BitmapFactory.decodeFile(imagePath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inPurgeable = true;
        bmOptions.inSampleSize = scaleFactor;

        Bitmap bitmap = BitmapFactory.decodeFile(imagePath, bmOptions);
        switch (imgAt) {
            case 1:
                imageView.setImageBitmap(bitmap);
                break;
            case 2:
                imageView.setImageBitmap(bitmap);
                break;
            case 3:
                imageView.setImageBitmap(bitmap);
                break;
            case 4:
                imageView.setImageBitmap(bitmap);
                break;
            case 5:
                imageView.setImageBitmap(bitmap);
                break;
            case 6:
                imageView.setImageBitmap(bitmap);
                break;
        }
    }

    public void takeLocation() {
        GPSTracker gps = new GPSTracker(getActivity());
        if (gps.canGetLocation()) {
            double latitudes = gps.getLatitude();
            double longitudes = gps.getLongitude();
            longitude.setText("Longitude : " + longitudes);
            latitude.setText("Latitude : " + latitudes);
            try {
                HomeActivity.realm.beginTransaction();
                ResultVisitSales item = HomeActivity.realm.where(ResultVisitSales.class).equalTo("id_visit", HomeActivity.dbLocal.getId_visit()).findFirst();
                item.setLat(latitudes + "");
                item.setLon(longitudes + "");
                HomeActivity.realm.commitTransaction();
            } catch (Exception e) {
                HomeActivity.realm.cancelTransaction();
            }
        } else {
            gps.showSettingsAlert();
        }
    }

    public void uploadData(ResultVisitSales result) {
        final ProgressDialog loading = ProgressDialog.show(getActivity(), "Mengirim data...", "Please wait...", false, false);
        AsyncController controller = new AsyncController(getActivity(), result) {
            @Override
            public void onAPIsuccess(String message) {
                loading.dismiss();
                saveToLocale(true);
                Log.e("JSON UPLOAD", message.toString());
            }

            @Override
            public void onAPIFailed(String errMessage) {
                loading.dismiss();
                if (errMessage.equals("null")) {
                    Snackbar.make(forsnack, "No internet connection", Snackbar.LENGTH_LONG).show();
                } else {
                    Snackbar.make(forsnack, "Upload gagal " + errMessage, Snackbar.LENGTH_LONG).show();
                }
            }

        };
        controller.setMethod(AsyncController.Method.UPLOAD_VISIT);
        controller.executeAPI();
    }


    public void savePenggunaanOli(final Equipment equipment, final int iteration, final int total) {
        final ProgressDialog loading = ProgressDialog.show(getActivity(), "Menyimpan data equipment " + iteration + "/" + total, "Please wait...", false, false);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.SAVE_PENGGUNAAN_OLI,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        ((HomeActivity) getActivity()).updateDataEquipment(equipment, 1);
                        iterator++;
                        if (iterator < arrEquipment.size()) {
                            savePenggunaanOli(arrEquipment.get(iterator), iterator, total);
                        } else {
                            uploadData(HomeActivity.dbLocal);
                        }
                        loading.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        loading.dismiss();
                        Snackbar.make(forsnack, "Upload data gagal.", Snackbar.LENGTH_LONG).show();

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();
                params.put("id_perusahaan", equipment.getId_perusahaan() + "");
                params.put("id_mesin", equipment.getId() + "");
                params.put("id_sales", equipment.getId_sales() + "");
                params.put("is_pertamina", equipment.getIs_pertamina());
                params.put("nama_pelumas", equipment.getNama_pelumas());
                params.put("volume", equipment.getVolume_kebutuhan());
                params.put("kapasitas_mesin", equipment.getKapasitas_mesin());
                params.put("id_visit_perusahaan", equipment.getId_visit_perusahaan() + "");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }


    public void saveToLocale(boolean isUpload) {
        try {
            if ((HomeActivity.dbLocal.getLat().equals("")) ||
                    (HomeActivity.dbLocal.getLon().equals("")) ||
                    (HomeActivity.dbLocal.getPhoto1().equals("")) ||
                    (HomeActivity.dbLocal.getPhoto2().equals("")) ||
                    (HomeActivity.dbLocal.getPhoto3().equals("")) ||
                    (HomeActivity.dbLocal.getPhoto4().equals("")) ||
                    (HomeActivity.dbLocal.getPhoto5().equals("")) ||
                    (HomeActivity.dbLocal.getPhoto6().equals(""))) {
                Snackbar.make(this.forsnack, "Anda belum mengisi seluruh data survey. Ambil lokasi dan foto survey terlebih dahulu.", Snackbar.LENGTH_LONG).show();
            } else {
                try {
                    ResultVisitSales localResultVisitSales = HomeActivity.realm.where(ResultVisitSales.class).equalTo("id_visit", HomeActivity.dbLocal.getId_visit()).findFirst();
                    HomeActivity.realm.beginTransaction();
                    localResultVisitSales.setIs_visit("1");
                    if (!isUpload) {
                        localResultVisitSales.setIsSent(0);
                        Toast.makeText(getActivity(), "Tesimpan di lokal", Toast.LENGTH_SHORT).show();
                    } else {
                        localResultVisitSales.setIsSent(1);
                        Toast.makeText(getActivity(), "Berhasil di upload", Toast.LENGTH_SHORT).show();
                    }
                    HomeActivity.realm.commitTransaction();
                    ((HomeActivity) getActivity()).displayView(2);
                } catch (Exception e) {
                    HomeActivity.realm.cancelTransaction();
                }
            }
        } catch (Exception e) {
            Snackbar.make(this.forsnack, "Anda belum mengisi seluruh data survey. Ambil lokasi dan foto survey terlebih dahulu.", Snackbar.LENGTH_LONG).show();
        }
    }


}
