package main.sopianaiskandar.net.lubricantssales.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import io.realm.RealmResults;
import main.sopianaiskandar.net.lubricantssales.AppController;
import main.sopianaiskandar.net.lubricantssales.Config;
import main.sopianaiskandar.net.lubricantssales.HomeActivity;
import main.sopianaiskandar.net.lubricantssales.LoginActivity;
import main.sopianaiskandar.net.lubricantssales.R;
import main.sopianaiskandar.net.lubricantssales.adapter.SessionManager;
import main.sopianaiskandar.net.lubricantssales.adapter.SyncronDaily;
import main.sopianaiskandar.net.lubricantssales.model.ResultVisit;
import main.sopianaiskandar.net.lubricantssales.model.ResultVisitSales;
import main.sopianaiskandar.net.lubricantssales.model.Rute;
import main.sopianaiskandar.net.lubricantssales.model.SalesProfile;

/**
 * Created by sopianaiskandar on 2/7/16.
 */
public class FragmentHome extends Fragment implements View.OnClickListener {
    private Button btn;
    private TextView isID,rute,update,tutupS,tutupP,bBisnis,notFound,isNewoo,isMc;
    public FragmentHome() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView =  inflater.inflate(R.layout.fragment_home, container, false);
        btn = (Button)rootView.findViewById(R.id.sync);
        btn.setOnClickListener(this);
        rute = (TextView)rootView.findViewById(R.id.title_colom1);
        update = (TextView)rootView.findViewById(R.id.title_colom2);
        tutupS = (TextView)rootView.findViewById(R.id.value_tutup_sementara);
        tutupP = (TextView)rootView.findViewById(R.id.value_tutup_permanens);
        bBisnis = (TextView)rootView.findViewById(R.id.value_isbisnis);
        notFound = (TextView)rootView.findViewById(R.id.value_nf);
        isNewoo = (TextView)rootView.findViewById(R.id.value_isnoos);
        isMc = (TextView)rootView.findViewById(R.id.value_ismersing);
        isID = (TextView)rootView.findViewById(R.id.title);
        dashboard();
        getTitle();
        return rootView;
    }

    public void getTitle(){
        RealmResults<SalesProfile> result = HomeActivity.realm.where(SalesProfile.class).findAll();
        Log.d("data profile",result.size()+" panjangnya");
        SessionManager sm = new SessionManager(getActivity());
        boolean found = false;
        for (SalesProfile s : result){
            if (s.getKodeSales().equals(sm.getCanvaserID())){
                isID.setText("Kode Sales    : "+s.getKodeSales()+"\n"+
                             "Nama Sales   : "+s.getNamaSales()+"\n"+
                             "Tanggal Visit : "+s.getTanggal());
                found = true;
            }
        }

        if (found == false){
            isID.setText("Kode Sales     : "+sm.getCanvaserID()+"\n"+
                         "Nama Sales    : "+sm.getCluster()+"\n"+
                         "Tanggal Visit  : "+"Belum Sinkron Rute");
        }
    }

    public void dashboard(){
        RealmResults<ResultVisitSales> result = HomeActivity.realm.where(ResultVisitSales.class).findAll();
        rute.setText("Rute : "+result.size());
        RealmResults<ResultVisit> visit = HomeActivity.realm.where(ResultVisit.class).findAll();
        int isbisnis=0,isnf=0,iscp=0,iscpp=0,isnoo = 0,ismercing=0,isu=0;
        for(ResultVisit c:visit) {
            if (c.getIsVisit() == 1) {
                if (c.getIsNoo() == 1) {
                    isnoo++;
                }

                if (c.getIsUpdate()==1){
                    isu++;
                }

                if (c.getIsMerchandise() == 1) {
                    ismercing++;
                }

                if (c.getIsBisnis() == 1) {
                    isbisnis++;
                }
                if (c.getIsNotFound() == 1) {
                    isnf++;
                }
                if (c.getIsClose() == 1) {
                    iscp++;
                }

                if (c.getIsCloseP() == 1) {
                    iscpp++;
                }

            }



        }
        this.isNewoo.setText("Noo : "+isnoo);
        this.update.setText("Update Data: "+isu);
        this.tutupS.setText("Tutup Sementara : "+iscp);
        this.tutupP.setText("Tutup Permanen : "+iscpp);
        this.notFound.setText("Tidak ditemukan: "+isnf);
        this.bBisnis.setText("Berubah Bisnis : "+isbisnis);
        this.isMc.setText("Merchandising : "+ismercing);
    }
    public void delete(){
        try{
        RealmResults<Rute> realmResults = HomeActivity.realm.where(Rute.class).findAll();
        HomeActivity.realm.beginTransaction();
        realmResults.clear();
        HomeActivity.realm.commitTransaction();
        }catch (Exception e){
            HomeActivity.realm.cancelTransaction();
        }
    }
    public void syncronize(){
        delete();
        SessionManager session = new SessionManager(getActivity());
        String id_personal = session.getCanvaserID();

        final ProgressDialog loading = ProgressDialog.show(getActivity(),"Downloading Task...","Please wait...",false,false);
        JsonArrayRequest req = new JsonArrayRequest(
                Config.API_URL+"/api-myroute?id_personal="+id_personal,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d("response my route", response.toString());
                        try {
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject outlet = (JSONObject) response.get(i);
                                HomeActivity.realm.beginTransaction();
                                Rute item = HomeActivity.realm.createObject(Rute.class);
                                item.setIdVisit(outlet.getInt("id_visit"));
                                item.setIdRoute(outlet.getInt("id_route"));
                                item.setIsVisit(outlet.getInt("is_visit"));
                                item.setIdTypeStore(outlet.getInt("id_typestore"));
                                item.setCodeCustomer(outlet.getString("code_customer"));
                                item.setNameCustomer(outlet.getString("name_customer"));
                                item.setAddress1(outlet.getString("address_1"));
                                item.setPhone(outlet.getString("phone"));
                                item.setLatCustomer(outlet.getString("lat_customer"));
                                item.setLongCustomer(outlet.getString("long_customer"));
                                item.setSpanduk(outlet.getString("spanduk"));
                                item.setFlag(outlet.getString("flag"));
                                item.setPoster(outlet.getString("poster"));
                                item.setMerchandise(outlet.getString("merchandise"));
                                item.setNamaPemilik(outlet.getString("nama_pemilik"));
                                item.setIdProv(outlet.getInt("id_prov"));
                                item.setProv(outlet.getString("prov"));
                                item.setIdKab(outlet.getInt("id_kab"));
                                item.setKab(outlet.getString("kab"));
                                item.setIdRegion(outlet.getInt("id_region"));
                                item.setKodeCluster(outlet.getString("kode_cluster"));
                                item.setNamaCluster(outlet.getString("nama_cluster"));
                                item.setIsNoo(outlet.getInt("is_noo"));
                                item.setIsUpdate(outlet.getInt("is_update"));
                                item.setIsNotFound(outlet.getInt("is_notfound"));
                                item.setIsBisnis(outlet.getInt("is_bisnis"));
                                item.setIsClose(outlet.getInt("is_close"));
                                item.setIsCloseP(outlet.getInt("is_close_p"));
                                item.setCodeSales(outlet.getString("code_sales"));
                                item.setIdRute(outlet.getInt("id_rute"));
                                item.setCurrentLat(outlet.getString("current_lat"));
                                item.setCurrentLon(outlet.getString("curren_lon"));
                                item.setMesinEdc(outlet.getInt("mesin_edc"));
                                item.setOliDari(outlet.getInt("oli_dari"));
                                item.setNooFromRoute(outlet.getInt("noo_from_rute"));
                                item.setJumlahFlag(outlet.getInt("jumlah_fg"));
                                item.setJumlahSpanduk(outlet.getInt("jumlah_spanduk"));
                                item.setJumlahPoster(outlet.getInt("jumlah_poster"));
                                item.setJumlahMerchandise(outlet.getInt("jumlah_merchandise"));
                                HomeActivity.realm.commitTransaction();
                            }
                        }catch (Exception e){
                            HomeActivity.realm.cancelTransaction();}
                        loading.dismiss();
                    }
                },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loading.dismiss();
            }
        }
        );
        AppController.getInstance().addToRequestQueue(req);

    }
    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.sync) {
            new SyncronDaily(getActivity(),this).syncronData();
        }
    }



}
