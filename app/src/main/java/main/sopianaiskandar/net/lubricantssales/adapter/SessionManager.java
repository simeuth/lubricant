package main.sopianaiskandar.net.lubricantssales.adapter;

/**
 * Created by sopianaiskandar on 2/11/16.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class SessionManager {
    private static String TAG = SessionManager.class.getSimpleName();

    //shared preferences
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "PrimeXL";
    private static final String KEY_IS_LOGGEDIN = "isLoggedIn";
    private static final String KEY_CANVASER_ID = "isCanvaserID";
    private static final String KEY_NAMA = "isNameSales";
    private static final String KEY_REGION = "isRegion";
    private static final String KEY_CLUSTER = "isCluster";

    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setLogin(boolean isLoggedIn,String canvaserID,String regionID,String cluserID,String nama) {

        editor.putBoolean(KEY_IS_LOGGEDIN, isLoggedIn);
        editor.putString(KEY_CANVASER_ID, canvaserID);
        editor.putString(KEY_REGION,regionID);
        editor.putString(KEY_CLUSTER,cluserID);
        editor.putString(KEY_NAMA,nama);
        // commit changes
        editor.commit();

        Log.d(TAG, "User login session modified!");
    }

    public boolean isLoggedIn(){
        return pref.getBoolean(KEY_IS_LOGGEDIN, false);
    }

    public String getRegion(){return pref.getString(KEY_REGION, "-");}

    public String getCluster(){return pref.getString(KEY_NAMA,"-");}

    public String getCanvaserID(){
        return pref.getString(KEY_CANVASER_ID, "-");
    }


}
