package main.sopianaiskandar.net.lubricantssales.adapter;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import main.sopianaiskandar.net.lubricantssales.AppController;

/**
 * Created by sopianaiskandar on 2/15/16.
 */
public class GetToken {
    public static String URL_TOKEN= "http://app3pl.pertaminalubricants.com/public/api-myroute?id_personal=10004";
    private String jsonResponse;
    private String token;
    public void getCsrfToken(){
        //RequestQueue queue = Volley.newRequestQueue(this);
        String url ="http://app3pl.pertaminalubricants.com/public/api-myroute?id_personal=10004";

// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        Log.d("test","Response is: "+ response);
                        //mTextView.setText("Response is: "+ response.substring(0,500));
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
               // mTextView.setText("That didn't work!");
            }
        });
// Add the request to the RequestQueue.
        //queue.add(stringRequest);

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
