package main.sopianaiskandar.net.lubricantssales.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import io.realm.RealmResults;
import main.sopianaiskandar.net.lubricantssales.Config;
import main.sopianaiskandar.net.lubricantssales.HomeActivity;
import main.sopianaiskandar.net.lubricantssales.MainActivity;
import main.sopianaiskandar.net.lubricantssales.Parser;
import main.sopianaiskandar.net.lubricantssales.R;
import main.sopianaiskandar.net.lubricantssales.adapter.AdapterListKabupaten;
import main.sopianaiskandar.net.lubricantssales.adapter.AdapterListProvinsi;
import main.sopianaiskandar.net.lubricantssales.adapter.SessionManager;
import main.sopianaiskandar.net.lubricantssales.model.Cluster;
import main.sopianaiskandar.net.lubricantssales.model.Industry;
import main.sopianaiskandar.net.lubricantssales.model.Provinsi;
import main.sopianaiskandar.net.lubricantssales.model.Region;
import main.sopianaiskandar.net.lubricantssales.model.ResultVisitSales;
import main.sopianaiskandar.net.lubricantssales.model.Segment;

/**
 * Created by sopianaiskandar on 2/7/16.
 */
@SuppressLint("ValidFragment")
public class FragmentOwner extends Fragment implements AdapterView.OnItemSelectedListener, View.OnClickListener {

    public static EditText alamatOutlet;
    public static EditText idRegion;
    public static EditText kodeOutlet;
    public static EditText namaPemilik;
    public static EditText nameOutlet;
    public static EditText phoneOutlet;
    ArrayAdapter<String> adapterSpinnerIndustri;
    ArrayAdapter<String> adapterSpinnerSegment;
    List<Industry> arrIndustry = new ArrayList();
    ArrayList<Region> arrKabupaten = new ArrayList();
    ArrayList<Provinsi> arrProvinsi = new ArrayList();
    List<Segment> arrSegment = new ArrayList();
    List<String> arrSpinnerIndustry = new ArrayList();
    List<String> arrSpinnerSegment = new ArrayList();
    public FloatingActionButton fabs;
    View forsnack;
    String idKabupaten;
    String idProvinsi = "";
    String idSales;
    Spinner jens_industri;
    MainActivity main;
    Spinner pembelian;
    Button setCluster;
    Button setKabupaten;
    Button setProvinsi;
    SessionManager sm;
    Spinner spinner_segment;
    public TextView valCluster;
    public TextView valKabupaten;
    public TextView valProvinsi;
    ViewPager viewPager;


    public FragmentOwner(MainActivity main, ViewPager viewPager) {
        this.main = main;
        this.viewPager = viewPager;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_owner, container, false);
        fabs = (FloatingActionButton) v.findViewById(R.id.button_upload_owner);
        fabs.setOnClickListener(this);
        sm = new SessionManager(getActivity());
        idSales = sm.getCanvaserID();
        setProvinsi = (Button) v.findViewById(R.id.set_provinsi);
        setProvinsi.setOnClickListener(this);
        setKabupaten = (Button) v.findViewById(R.id.set_kabupaten);
        setKabupaten.setOnClickListener(this);
        setCluster = (Button) v.findViewById(R.id.set_cluster);
        setCluster.setOnClickListener(this);
        valProvinsi = (TextView) v.findViewById(R.id.val_provinsi);
        valKabupaten = (TextView) v.findViewById(R.id.val_kabupaten);
        valCluster = (TextView) v.findViewById(R.id.val_cluster);
        idRegion = (EditText) v.findViewById(R.id.region_outlet);
        idRegion.setEnabled(false);
        nameOutlet = (EditText) v.findViewById(R.id.nama_outlet);
        kodeOutlet = (EditText) v.findViewById(R.id.kode_outlet);
        namaPemilik = (EditText) v.findViewById(R.id.nama_pemiliks);
        alamatOutlet = (EditText) v.findViewById(R.id.alamat_outlet);
        phoneOutlet = (EditText) v.findViewById(R.id.phone_outlet);


        arrSpinnerIndustry.add("-Memuat Data-");
        jens_industri = (Spinner) v.findViewById(R.id.jens_industri);
        adapterSpinnerIndustri = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, arrSpinnerIndustry);
        adapterSpinnerIndustri.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        jens_industri.setAdapter(adapterSpinnerIndustri);


        arrSpinnerSegment.add("-Pilih Industri-");
        spinner_segment = (Spinner) v.findViewById(R.id.spinner_segment);
        adapterSpinnerSegment = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, arrSpinnerSegment);
        adapterSpinnerSegment.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_segment.setAdapter(adapterSpinnerSegment);


        getProvinsi();
        getKabupaten(HomeActivity.dbLocal.getId_provinsi());
        getJenisIndustri();
        Log.e("id_COOY1", HomeActivity.dbLocal.getId_jenis_industri() + "");
        Log.e("id_COOY2", HomeActivity.dbLocal.getId_segment() + "");
        getSegment(HomeActivity.dbLocal.getId_jenis_industri());

        try {
            setSpinnerJenisIndustri(jens_industri, checkNull(HomeActivity.dbLocal.getId_jenis_industri() + ""));
            setSpinnerSegment(spinner_segment, checkNull(HomeActivity.dbLocal.getId_segment() + ""));
            kodeOutlet.setText(checkNull(HomeActivity.dbLocal.getId_perusahaan() + ""));
            nameOutlet.setText(checkNull(HomeActivity.dbLocal.getPerusahaan()));
            kodeOutlet.setEnabled(false);
            alamatOutlet.setText(checkNull(HomeActivity.dbLocal.getAlamat()));
            phoneOutlet.setText(checkNull(HomeActivity.dbLocal.getNo_hp()));
            namaPemilik.setText(checkNull(HomeActivity.dbLocal.getContact()));
            valProvinsi.setText(initProvinsi(checkNull(HomeActivity.dbLocal.getId_provinsi() + "")));
            valKabupaten.setText(initkabupaten(checkNull(HomeActivity.dbLocal.getId_kabupaten() + "")));
            forsnack = v;
            idProvinsi = checkNull(HomeActivity.dbLocal.getId_provinsi() + "");
            Snackbar.make(this.forsnack, "Profile Perusahaan", Snackbar.LENGTH_LONG).show();
        } catch (Exception localException) {

            Log.e("Exception profile", localException.toString());
        }


        return v;
    }


    public String checkNull(String paramString) {
        try {
            if ((paramString.trim().equals("null")) || (paramString.trim().equals("")) || (paramString == null)) {
                return "";
            } else {
                return paramString;
            }
        } catch (Exception localException) {
            return "";
        }
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.button_upload_owner:
                saveContent();
                break;
            case R.id.set_provinsi:
                openDialog();
                break;
            case R.id.set_kabupaten:
                openDialogKab();
                break;
        }
    }


    private Dialog d_prov;
    private ListView lv;
    AdapterListProvinsi adapter;

    public void openDialog() {
        d_prov = new Dialog(getActivity());
        d_prov.setTitle("Provinsi");
        d_prov.setContentView(R.layout.dialog_list_provinsi);
        lv = (ListView) d_prov.findViewById(R.id.list_prov);
        adapter = new AdapterListProvinsi(getActivity(), arrProvinsi);
        lv.setAdapter(adapter);

        EditText edit = (EditText) d_prov.findViewById(R.id.input_search_provinsi);
        edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                valProvinsi.setText(adapter.getData().get(position).getNama());
                ResultVisitSales item = HomeActivity.realm.where(ResultVisitSales.class).equalTo("id_visit", HomeActivity.dbLocal.getId_visit()).findFirst();
                try {
                    HomeActivity.realm.beginTransaction();
                    item.setId_provinsi(adapter.getData().get(position).getId());
                    item.setId_kabupaten(0);
                    valKabupaten.setText("-");
                    HomeActivity.realm.commitTransaction();
                } catch (Exception e) {
                    HomeActivity.realm.cancelTransaction();
                }
                HomeActivity.dbLocal = item;
                d_prov.dismiss();
            }
        });
        d_prov.show();
    }


    private Dialog d_kab;
    private ListView lv_k;
    AdapterListKabupaten adapter_k;

    public void openDialogKab() {
        d_kab = new Dialog(getActivity());
        d_kab.setTitle("Kabupaten");
        d_kab.setContentView(R.layout.dialog_list_provinsi);
        lv_k = (ListView) d_kab.findViewById(R.id.list_prov);
        adapter_k = new AdapterListKabupaten(getActivity(), arrKabupaten);
        lv_k.setAdapter(adapter_k);

        getKabupaten(HomeActivity.dbLocal.getId_provinsi());

        EditText edit = (EditText) d_kab.findViewById(R.id.input_search_provinsi);
        edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adapter_k.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        lv_k.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                valKabupaten.setText(adapter_k.getData().get(position).getName());
                ResultVisitSales item = HomeActivity.realm.where(ResultVisitSales.class).equalTo("id_visit", HomeActivity.dbLocal.getId_visit()).findFirst();
                try {
                    HomeActivity.realm.beginTransaction();
                    item.setId_kabupaten(adapter_k.getData().get(position).getId());
                    HomeActivity.realm.commitTransaction();
                } catch (Exception e) {
                    HomeActivity.realm.cancelTransaction();
                }
                HomeActivity.dbLocal = item;
                d_kab.dismiss();
            }
        });
        d_kab.show();
    }


    public void saveContent() {
        if (validator()) {
            ResultVisitSales localResultVisitSales = HomeActivity.realm.where(ResultVisitSales.class).equalTo("id_visit", HomeActivity.dbLocal.getId_visit()).findFirst();
            try {
                HomeActivity.realm.beginTransaction();
                localResultVisitSales.setId_sales(Integer.valueOf(idSales));
                localResultVisitSales.setContact(namaPemilik.getText().toString());
                localResultVisitSales.setAlamat(alamatOutlet.getText().toString());
                localResultVisitSales.setPerusahaan(nameOutlet.getText().toString());
                localResultVisitSales.setNo_hp(phoneOutlet.getText().toString());
                localResultVisitSales.setId_jenis_industri(arrIndustry.get(jens_industri.getSelectedItemPosition() - 1).getId());
                Log.e("id_industri", arrIndustry.get(jens_industri.getSelectedItemPosition() - 1).getId() + "");
                Log.e("id_segment", arrSegment.get(spinner_segment.getSelectedItemPosition() - 1).getId() + "");
                localResultVisitSales.setId_segment(arrSegment.get(spinner_segment.getSelectedItemPosition() - 1).getId());
                HomeActivity.realm.commitTransaction();
                Snackbar.make(forsnack, "Sukses Simpan", Snackbar.LENGTH_LONG).show();
                HomeActivity.dbLocal = localResultVisitSales;
                viewPager.setCurrentItem(1);
            } catch (Exception localException) {
                HomeActivity.realm.cancelTransaction();
                Log.e("eksepsi update", localException.getMessage());
                Snackbar.make(this.forsnack, "Gagal Simpan Harap isi seluruh from isian sebelum melanjutkan.", Snackbar.LENGTH_LONG).show();
                return;
            }
        }
        Snackbar.make(this.forsnack, "Harap isi seluruh from isian sebelum melanjutkan.", Snackbar.LENGTH_LONG).show();
    }

    public boolean validator() {
        if (nameOutlet.getText().toString().trim().equals("")) {
            Snackbar.make(this.forsnack, "Nama Perusahaan Belum Di Isi", Snackbar.LENGTH_LONG).show();
            return false;
        }
        if (namaPemilik.getText().toString().trim().equals("")) {
            Snackbar.make(this.forsnack, "Nama Pemilik Outlet Belum Di Isi", Snackbar.LENGTH_LONG).show();
            return false;
        }
        if ((alamatOutlet.getText().toString().trim().equals("-")) || (alamatOutlet.getText().toString().trim().equals(""))) {
            Snackbar.make(this.forsnack, "Alamat Perusahaan  Belum Di Isi", Snackbar.LENGTH_LONG).show();
            return false;
        }
        if (phoneOutlet.getText().toString().trim().equals("")) {
            Snackbar.make(this.forsnack, "Nomor Telepon Belum Di Isi", Snackbar.LENGTH_LONG).show();
            return false;
        }

        if (HomeActivity.dbLocal.getId_kabupaten() == 0) {
            Snackbar.make(this.forsnack, "Kabubaten belum di pilih", Snackbar.LENGTH_LONG).show();
            return false;
        }
        return true;
    }


    public void getJenisIndustri() {
        try {
            arrSpinnerIndustry.clear();
            arrIndustry.clear();
            arrSpinnerIndustry.add("-Pilih-");
            RealmResults<Industry> results = HomeActivity.realm.where(Industry.class).findAll();
            for (Industry i : results) {
                Industry item = new Industry();
                item.setNama(i.getNama());
                item.setId(i.getId());
                arrIndustry.add(item);
                arrSpinnerIndustry.add(item.getNama());
            }
            adapterSpinnerIndustri.notifyDataSetChanged();
            setListenerSpinnerIndustry();
            return;
        } catch (Exception localException) {
        }
    }

    public void getKabupaten(int paramInt) {
        try {
            arrKabupaten.clear();
            RealmResults<Region> results = HomeActivity.realm.where(Region.class).equalTo("id_provinsi", Integer.valueOf(paramInt)).findAll();
            for (Region i : results) {
                Region item = new Region();
                item.setName(i.getName());
                item.setId(i.getId());
                item.setId_provinsi(i.getId_provinsi());
                arrKabupaten.add(item);
            }
            adapter_k.notifyDataSetChanged();

        } catch (Exception localException1) {
        }

    }


    public void getProvinsi() {
        try {
            arrProvinsi.clear();
            RealmResults<Provinsi> results = HomeActivity.realm.where(Provinsi.class).findAll();
            for (Provinsi i : results) {
                Provinsi item = new Provinsi();
                item.setNama(i.getNama());
                item.setId(i.getId());
                this.arrProvinsi.add(item);
            }
            adapter.notifyDataSetChanged();
        } catch (Exception localException1) {
        }
    }

    public void getSegment(int id_jenis_industri) {
        try {
            arrSpinnerSegment.clear();
            arrSegment.clear();
            arrSpinnerSegment.add("-Pilih-");
            RealmResults<Segment> results = HomeActivity.realm.where(Segment.class).equalTo("id_jenis_industri", id_jenis_industri).findAll();
            for (Segment i : results) {
                Segment item = new Segment();
                item.setId(i.getId());
                item.setNama(i.getNama());
                arrSegment.add(item);
                arrSpinnerSegment.add(item.getNama());
            }
            spinner_segment.setSelection(0);
            adapterSpinnerSegment.notifyDataSetChanged();
            setSpinnerSegment(spinner_segment, checkNull(HomeActivity.dbLocal.getId_segment() + ""));
        } catch (Exception localException) {
        }
    }


    public String initProvinsi(String paramString) {
        String str = "-";
        for (int i = 0; i < arrProvinsi.size(); i++) {
            if (paramString.equals(arrProvinsi.get(i).getId() + "")) {
                str = arrProvinsi.get(i).getNama();
            }
        }
        return str;
    }

    public String initkabupaten(String paramString) {
        String str = "-";
        for (int i = 0; i < arrKabupaten.size(); i++) {
            if (paramString.equals(arrKabupaten.get(i).getId() + "")) {
                str = arrKabupaten.get(i).getName();
            }
        }
        return str;
    }


    void setListenerSpinnerIndustry() {
        jens_industri.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong) {
                if (arrIndustry.size() > 0) {
                    if (jens_industri.getSelectedItemPosition() > 0) {
                        getSegment(arrIndustry.get(jens_industri.getSelectedItemPosition() - 1).getId());
                    } else {
                        spinner_segment.setSelection(0);
                    }
                }
            }

            public void onNothingSelected(AdapterView<?> paramAdapterView) {
            }
        });
    }

    public void setSpinnerJenisIndustri(Spinner paramSpinner, String paramString) {
        for (int i = 0; i < arrIndustry.size(); i++) {
            if (paramString.equals(arrIndustry.get(i).getId() + "")) {
                paramSpinner.setSelection(i + 1);
            }
        }
    }

    public void setSpinnerSegment(Spinner paramSpinner, String paramString) {
        for (int i = 0; i < arrSegment.size(); i++) {
            if (paramString.equals(arrSegment.get(i).getId() + "")) {
                paramSpinner.setSelection(i + 1);
            }
        }
    }


}


