package main.sopianaiskandar.net.lubricantssales.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by sopianaiskandar on 2/21/16.
 */
public class Cluster extends RealmObject {

    @PrimaryKey
    private String codeBranch;
    private String nameBranch;

    public Cluster() {
    }

    public String getCodeBranch() {
        return codeBranch;
    }

    public void setCodeBranch(String codeBranch) {
        this.codeBranch = codeBranch;
    }

    public String getNameBranch() {
        return nameBranch;
    }

    public void setNameBranch(String nameBranch) {
        this.nameBranch = nameBranch;
    }
}
