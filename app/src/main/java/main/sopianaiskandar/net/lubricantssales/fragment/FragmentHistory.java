package main.sopianaiskandar.net.lubricantssales.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import io.realm.RealmResults;
import main.sopianaiskandar.net.lubricantssales.HomeActivity;
import main.sopianaiskandar.net.lubricantssales.R;
import main.sopianaiskandar.net.lubricantssales.model.ResultVisit;

/**
 * Created by sopianaiskandar on 3/5/16.
 */
public class FragmentHistory extends Fragment {

    ArrayAdapter<String> adapter;
    private ListView lv;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview =  inflater.inflate(R.layout.fragment_history, container, false);
        lv  = (ListView) rootview.findViewById(R.id.list_history);
        setAdapter();
        return rootview;
    }

    public void setAdapter(){
        ArrayList<String> stringArray = new ArrayList<String>();
        RealmResults<ResultVisit> visit = HomeActivity.realm.where(ResultVisit.class).findAll();
        for(ResultVisit c:visit) {
            if (c.getIsVisit() == 1){
                String status = "outlet update data";
                if (c.getIsNoo() == 1){
                    status =        "new open outlet";
                }
                if (c.getIsMerchandise() == 1){
                    status = "merchandising";
                }

                if (c.getIsBisnis() == 1){
                    status = "outlet berubah bisnis";
                }
                if (c.getIsNotFound() == 1){
                    status = "outlet tidak diketemukan";
                }
                if (c.getIsClose() == 1){
                    status = "outlet tutup sementara";
                }

                if (c.getIsCloseP() == 1){
                    status = "outlet tutup permanen";
                }

                stringArray.add(c.getCodeCustomer() + "-" + c.getNameCustomer() + "\n" + c.getAddress1() + "\n" + status);
            }
        }
        adapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1,stringArray);
        lv.setAdapter(adapter);

    }

}
