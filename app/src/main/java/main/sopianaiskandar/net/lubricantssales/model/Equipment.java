package main.sopianaiskandar.net.lubricantssales.model;

import io.realm.RealmObject;

public class Equipment extends RealmObject {
    private int id;
    private int id_perusahaan;
    private int id_sales;
    private int id_visit_perusahaan;
    private int isSent;
    private String is_pertamina;
    private String kapasitas_mesin;
    private String nama;
    private String nama_pelumas;
    private String volume_kebutuhan;

    public int getId() {
        return this.id;
    }

    public int getId_perusahaan() {
        return this.id_perusahaan;
    }

    public int getId_sales() {
        return this.id_sales;
    }

    public int getId_visit_perusahaan() {
        return this.id_visit_perusahaan;
    }

    public int getIsSent() {
        return this.isSent;
    }

    public String getIs_pertamina() {
        return this.is_pertamina;
    }

    public String getKapasitas_mesin() {
        return this.kapasitas_mesin;
    }

    public String getNama() {
        return this.nama;
    }

    public String getNama_pelumas() {
        return this.nama_pelumas;
    }

    public String getVolume_kebutuhan() {
        return this.volume_kebutuhan;
    }

    public void setId(int paramInt) {
        this.id = paramInt;
    }

    public void setId_perusahaan(int paramInt) {
        this.id_perusahaan = paramInt;
    }

    public void setId_sales(int paramInt) {
        this.id_sales = paramInt;
    }

    public void setId_visit_perusahaan(int paramInt) {
        this.id_visit_perusahaan = paramInt;
    }

    public void setIsSent(int paramInt) {
        this.isSent = paramInt;
    }

    public void setIs_pertamina(String paramString) {
        this.is_pertamina = paramString;
    }

    public void setKapasitas_mesin(String paramString) {
        this.kapasitas_mesin = paramString;
    }

    public void setNama(String paramString) {
        this.nama = paramString;
    }

    public void setNama_pelumas(String paramString) {
        this.nama_pelumas = paramString;
    }

    public void setVolume_kebutuhan(String paramString) {
        this.volume_kebutuhan = paramString;
    }
}