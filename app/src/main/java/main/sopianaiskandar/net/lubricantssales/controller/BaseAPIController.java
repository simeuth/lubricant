/**
 * Base API Controller
 *
 * @author egiadtya
 * 27 October 2014
 */
package main.sopianaiskandar.net.lubricantssales.controller;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.UrlQuerySanitizer;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.MySSLSocketFactory;
import com.loopj.android.http.RequestParams;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.HashMap;

import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import main.sopianaiskandar.net.lubricantssales.callback.JSONParserInterface;
import main.sopianaiskandar.net.lubricantssales.callback.OnCallAPI;


public abstract class BaseAPIController implements OnCallAPI, JSONParserInterface {
    public static final String EMPTY_ENDPOINT = "";
    protected AsyncHttpClient client;
    protected Context context;
    private HashMap<String, String> paramMap;
    protected String errorMessage;
    protected SharedPreferences preferences;
    protected Editor editor;

    public BaseAPIController(Context context) {
        try {
            // We initialize a default Keystore
            KeyStore trustStore = null;
            trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            // We load the KeyStore
            trustStore.load(null, null);
            // We initialize a new SSLSocketFacrory
            MySSLSocketFactory socketFactory = new MySSLSocketFactory(trustStore);
            // We set that all host names are allowed in the socket factory
            socketFactory.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            if (client == null) {
                client = new AsyncHttpClient();
                client.setSSLSocketFactory(socketFactory);
                client.setMaxRetriesAndTimeout(AsyncHttpClient.DEFAULT_MAX_RETRIES, 10 * 1000);
            }
            paramMap = new HashMap<String, String>();
            this.context = context;
            preferences = PreferenceManager.getDefaultSharedPreferences(context);
            editor = preferences.edit();

        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (UnrecoverableKeyException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
    }

    public void GET(String url, String endpoint) {
        url = url + endpoint;
        RequestParams params;
        if (paramMap.size() > 0) {
            params = new RequestParams(paramMap);
            client.get(url, params, responseHandler);
            url = AsyncHttpClient.getUrlWithQueryString(false, url, params);
        } else {
            client.get(url, responseHandler);
        }

    }


    public void POST_WITH_MANY_IMAGE(String endpoint, ArrayList<FileAttachment> attachment) {
        RequestParams params;
        if (paramMap.size() > 0) {
            params = new RequestParams(paramMap);
            try {
                params.put("_method", "POST");
                for (int i = 0; i < attachment.size(); i++) {
                    params.put(attachment.get(i).getKey(), attachment.get(i).getFile());
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            client.post(endpoint, params, responseHandler);
        } else {
            client.post(endpoint, responseHandler);
        }
    }


    private AsyncHttpResponseHandler responseHandler = new AsyncHttpResponseHandler() {
        @Override
        public void onCancel() {
            super.onCancel();
        }

        @Override
        public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
            UrlQuerySanitizer sanitizer = new UrlQuerySanitizer();
            String content = new String(responseBody);
            switch (statusCode) {
                case 200:
                    try {
                        onAPIsuccess(sanitizer.unescape(content));
                        Log.i("onAPIsuccess", sanitizer.unescape(content));
                    } catch (Exception e) {
                        e.printStackTrace();
                        onAPIFailed(e.getMessage());
                    }
                    break;
                case 201:
                    try {
                        onAPIsuccess(sanitizer.unescape(content));
                        Log.i("onAPIsuccess", sanitizer.unescape(content));
                    } catch (Exception e) {
                        e.printStackTrace();
                        onAPIFailed(e.getMessage());
                    }
                    break;
                case 202:
                    try {
                        onAPIsuccess(sanitizer.unescape(content));
                        Log.i("onAPIsuccess", sanitizer.unescape(content));
                    } catch (Exception e) {
                        e.printStackTrace();
                        onAPIFailed(e.getMessage());
                    }
                    break;
                default:
                    onAPIFailed("Something when wrong");
                    break;
            }

        }

        @Override
        public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
            try {
                UrlQuerySanitizer sanitizer = new UrlQuerySanitizer();
                String content = new String(responseBody);
                switch (statusCode) {
                    case 409:
                        try {
                            onAPIsuccess(sanitizer.unescape(content));
                            Log.i("onAPIsuccess", sanitizer.unescape(content));
                        } catch (Exception e) {
                            e.printStackTrace();
                            onAPIFailed(e.getMessage());
                        }
                        break;
                    case 400:
                        onAPIFailed("Bad Request");
                        break;
                    case 403:
                        onAPIFailed("403");
                        break;
                    case 401:
                        onAPIFailed("Client Error Unauthorized");
                        break;
                    case 404:
                        onAPIFailed("404");
                        break;
                    case 500:
                        onAPIFailed("Server Error Internal Server Error");
                        break;
                    case 503:
                        onAPIFailed("Server Error Service Unavailable");
                        break;
                    default:
                        onAPIFailed("Something when wrong");
                        break;
                }
            } catch (NullPointerException e) {
                try {
                    if (statusCode == 304) {
                        onAPIFailed("404");
                    } else {
                        onAPIFailed("null");
                    }
                } catch (Exception err) {

                }
            }

        }

    };

    @Override
    public void onAPIFailed(String errorMessage) {
        Toast.makeText(context, errorMessage, Toast.LENGTH_LONG).show();
        ((DefaultHttpClient) client.getHttpClient()).close();
    }


    @Override
    public void onAPIsuccess(String errorMessage) {
        ((DefaultHttpClient) client.getHttpClient()).close();
    }


    public void addParameter(String key, String value) {
        if (paramMap != null) {
            paramMap.put(key, value);
        }
    }


    public AsyncHttpClient getClient() {
        return this.client;
    }


    public class FileAttachment {
        private String key;
        private File file;
        private String title;
        private String contentType;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getContentType() {
            return contentType;
        }

        public void setContentType(String contentType) {
            this.contentType = contentType;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public File getFile() {
            return file;
        }

        public void setFile(File file) {
            this.file = file;
        }
    }

}
