package main.sopianaiskandar.net.lubricantssales.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

import io.realm.RealmResults;
import main.sopianaiskandar.net.lubricantssales.Config;
import main.sopianaiskandar.net.lubricantssales.HomeActivity;
import main.sopianaiskandar.net.lubricantssales.R;
import main.sopianaiskandar.net.lubricantssales.controller.AsyncController;
import main.sopianaiskandar.net.lubricantssales.model.Equipment;
import main.sopianaiskandar.net.lubricantssales.model.Industry;
import main.sopianaiskandar.net.lubricantssales.model.Provinsi;
import main.sopianaiskandar.net.lubricantssales.model.Region;
import main.sopianaiskandar.net.lubricantssales.model.ResultVisit;
import main.sopianaiskandar.net.lubricantssales.model.ResultVisitSales;
import main.sopianaiskandar.net.lubricantssales.model.Rute;
import main.sopianaiskandar.net.lubricantssales.model.SalesProfile;
import main.sopianaiskandar.net.lubricantssales.model.Segment;

/**
 * Created by sopianaiskandar on 2/12/16.
 */

public class FragmentSales extends Fragment {

    private ArrayList<ResultVisitSales> arrResultVisit = new ArrayList();
    private Button close;
    private ArrayList<Equipment> dataEquipment = new ArrayList();
    private int out, send;
    private TextView sent, outbox;
    private int iterartorVisit = 0;
    private int iteratorEquipment = 0;
    private View forsnack;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.cd_dashboard, container, false);
        close = (Button) rootview.findViewById(R.id.cd_closing_button);
        sent = (TextView) rootview.findViewById(R.id.title_colom1);
        outbox = (TextView) rootview.findViewById(R.id.save_proses_status);
        calculate();
        calculateEquipment();
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculateEquipment();
                if (dataEquipment.size() > 0) {
                    iteratorEquipment = 0;
                    savePenggunaanOli(dataEquipment.get(iterartorVisit), iteratorEquipment, dataEquipment.size());
                } else {
                    calculate();
                    if (arrResultVisit.size() > 0) {
                        iterartorVisit = 0;
                        uploadData(arrResultVisit.get(iterartorVisit));
                    } else {
                        confirmation();
                    }
                }
            }
        });
        forsnack = rootview;
        return rootview;
    }

    public void calculate() {
        send = 0;
        out = 0;
        arrResultVisit.clear();
        RealmResults<ResultVisitSales> visit = HomeActivity.realm.where(ResultVisitSales.class).findAll();
        for (ResultVisitSales c : visit) {
            if (c.getIsSent() == 1) {
                send = send + 1;
            } else {
                if (c.getIs_visit().equals("1")) {
                    out = out + 1;
                    arrResultVisit.add(c);
                }
            }
        }
        sent.setText("terkirim " + send);
        outbox.setText("tersimpan " + out);
    }

    public void calculateCount() {
        send = 0;
        out = 0;
        RealmResults<ResultVisitSales> visit = HomeActivity.realm.where(ResultVisitSales.class).findAll();
        for (ResultVisitSales c : visit) {
            if (c.getIsSent() == 1) {
                send = send + 1;
            } else {
                if (c.getIs_visit().equals("1")) {
                    out = out + 1;
                }
            }
        }
        sent.setText("terkirim " + send);
        outbox.setText("tersimpan " + out);
    }


    public void calculateEquipment() {
        try {
            dataEquipment.clear();
            RealmResults<Equipment> results = HomeActivity.realm.where(Equipment.class).findAll();
            for (Equipment e : results) {
                if (e.getIsSent() == 0) {
                    Equipment equipment = new Equipment();
                    equipment.setId_perusahaan(e.getId_perusahaan());
                    equipment.setId(e.getId());
                    equipment.setId_sales(e.getId_sales());
                    equipment.setIs_pertamina(e.getIs_pertamina());
                    equipment.setNama_pelumas(e.getNama_pelumas());
                    equipment.setVolume_kebutuhan(e.getVolume_kebutuhan());
                    equipment.setKapasitas_mesin(e.getKapasitas_mesin());
                    equipment.setId_visit_perusahaan(e.getId_visit_perusahaan());
                    dataEquipment.add(equipment);
                }
            }
        } catch (Exception e) {

        }
    }


    public void uploadData(ResultVisitSales result) {
        final ProgressDialog loading = ProgressDialog.show(getActivity(), "Mengirim data...", "Please wait...", false, false);
        try {
            ResultVisitSales localResultVisitSales = getData(arrResultVisit.get(iterartorVisit).getId_visit());
            HomeActivity.dbLocal = localResultVisitSales;
            AsyncController controller = new AsyncController(getActivity(), result) {
                @Override
                public void onAPIsuccess(String message) {
                    loading.dismiss();
                    ((HomeActivity) getActivity()).setIsUploaded();
                    calculateCount();
                    iterartorVisit++;
                    if (iterartorVisit < arrResultVisit.size()) {
                        uploadData(arrResultVisit.get(iterartorVisit));
                    } else {
                        confirmation();
                    }
                    Log.e("JSON UPLOAD", message.toString());
                }

                @Override
                public void onAPIFailed(String errMessage) {
                    loading.dismiss();
                    if (errMessage.equals("null")) {
                        Snackbar.make(forsnack, "No internet connection", Snackbar.LENGTH_LONG).show();
                    } else {
                        Snackbar.make(forsnack, "Upload gagal " + errMessage, Snackbar.LENGTH_LONG).show();
                    }
                }

            };
            controller.setMethod(AsyncController.Method.UPLOAD_VISIT);
            controller.executeAPI();
        } catch (Exception e) {
            Snackbar.make(forsnack, "Upload gagal", Snackbar.LENGTH_LONG).show();
            loading.dismiss();
        }

    }


    public void savePenggunaanOli(final Equipment equipment, final int iteration, final int total) {
        final ProgressDialog loading = ProgressDialog.show(getActivity(), "Menyimpan data equipment " + iteration + "/" + total, "Please wait...", false, false);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.SAVE_PENGGUNAAN_OLI,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        ((HomeActivity) getActivity()).updateDataEquipment(equipment, 1);
                        iteratorEquipment++;
                        if (iteratorEquipment < dataEquipment.size()) {
                            savePenggunaanOli(dataEquipment.get(iteratorEquipment), iteratorEquipment, total);
                        } else {
                            if (arrResultVisit.size() > 0) {
                                iterartorVisit = 0;
                                uploadData(arrResultVisit.get(iterartorVisit));
                            }
                        }
                        loading.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        loading.dismiss();
                        Snackbar.make(forsnack, "Upload data gagal.", Snackbar.LENGTH_LONG).show();

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();
                params.put("id_perusahaan", equipment.getId_perusahaan() + "");
                params.put("id_mesin", equipment.getId() + "");
                params.put("id_sales", equipment.getId_sales() + "");
                params.put("is_pertamina", equipment.getIs_pertamina());
                params.put("nama_pelumas", equipment.getNama_pelumas());
                params.put("volume", equipment.getVolume_kebutuhan());
                params.put("kapasitas_mesin", equipment.getKapasitas_mesin());
                params.put("id_visit_perusahaan", equipment.getId_visit_perusahaan() + "");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

    public void confirmation() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("closing akan menghapus seluruh data, apakah anda yakin ?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (out > 0) {
                            Snackbar.make(getView(), "masih ada data yang tersimpan di device , silahkan kirim dahulu " +
                                    "diriwayat kunjungan", Snackbar.LENGTH_SHORT).show();
                        } else {
                            calculateCount();
                            delete();
                            profilehapus();
                            deleteEquipment();
                            jenisIndustriDelete();
                            segmentDelete();
                            provinsiDelete();
                            kabupatenDelete();
                        }
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }


    public void delete() {
        try {
            RealmResults<ResultVisitSales> realmResults = HomeActivity.realm.where(ResultVisitSales.class).findAll();
            HomeActivity.realm.beginTransaction();
            realmResults.clear();
            HomeActivity.realm.commitTransaction();
        } catch (Exception e) {
            HomeActivity.realm.cancelTransaction();
        }
    }

    public void profilehapus() {
        try {
            RealmResults<SalesProfile> realmResults = HomeActivity.realm.where(SalesProfile.class).findAll();
            HomeActivity.realm.beginTransaction();
            realmResults.clear();
            HomeActivity.realm.commitTransaction();
        } catch (Exception e) {
            HomeActivity.realm.cancelTransaction();
        }
    }


    public void deleteEquipment() {
        try {
            RealmResults<Equipment> realmResults = HomeActivity.realm.where(Equipment.class).findAll();
            HomeActivity.realm.beginTransaction();
            realmResults.clear();
            HomeActivity.realm.commitTransaction();
        } catch (Exception e) {
            HomeActivity.realm.cancelTransaction();
        }
    }



    public void provinsiDelete() {
        try {
            RealmResults<Provinsi> realmResults = HomeActivity.realm.where(Provinsi.class).findAll();
            HomeActivity.realm.beginTransaction();
            realmResults.clear();
            HomeActivity.realm.commitTransaction();
        } catch (Exception e) {
            HomeActivity.realm.cancelTransaction();
        }
    }

    public void kabupatenDelete() {
        try {
            RealmResults<Region> realmResults = HomeActivity.realm.where(Region.class).findAll();
            HomeActivity.realm.beginTransaction();
            realmResults.clear();
            HomeActivity.realm.commitTransaction();
        } catch (Exception e) {
            HomeActivity.realm.cancelTransaction();
        }
    }

    public void jenisIndustriDelete() {
        try {
            RealmResults<Industry> realmResults = HomeActivity.realm.where(Industry.class).findAll();
            HomeActivity.realm.beginTransaction();
            realmResults.clear();
            HomeActivity.realm.commitTransaction();
        } catch (Exception e) {
            HomeActivity.realm.cancelTransaction();
        }
    }

    public void segmentDelete() {
        try {
            RealmResults<Segment> realmResults = HomeActivity.realm.where(Segment.class).findAll();
            HomeActivity.realm.beginTransaction();
            realmResults.clear();
            HomeActivity.realm.commitTransaction();
        } catch (Exception e) {
            HomeActivity.realm.cancelTransaction();
        }
    }


    public ResultVisitSales getData(int paramInt) {
        ResultVisitSales localResultVisitSales1 = new ResultVisitSales();
        ResultVisitSales localResultVisitSales2 = HomeActivity.realm.where(ResultVisitSales.class).equalTo("id_visit", Integer.valueOf(paramInt)).findFirst();
        localResultVisitSales1.setPhoto1(localResultVisitSales2.getPhoto1());
        localResultVisitSales1.setPhoto2(localResultVisitSales2.getPhoto2());
        localResultVisitSales1.setPhoto3(localResultVisitSales2.getPhoto3());
        localResultVisitSales1.setPhoto4(localResultVisitSales2.getPhoto4());
        localResultVisitSales1.setPhoto5(localResultVisitSales2.getPhoto5());
        localResultVisitSales1.setPhoto6(localResultVisitSales2.getPhoto6());
        localResultVisitSales1.setId_visit(localResultVisitSales2.getId_visit());
        localResultVisitSales1.setContact(localResultVisitSales2.getContact());
        localResultVisitSales1.setLat(localResultVisitSales2.getLat());
        localResultVisitSales1.setLon(localResultVisitSales2.getLon());
        localResultVisitSales1.setId_sales(localResultVisitSales2.getId_sales());
        localResultVisitSales1.setIs_visit(localResultVisitSales2.getIs_visit());
        localResultVisitSales1.setNo_hp(localResultVisitSales2.getNo_hp());
        localResultVisitSales1.setId_perusahaan(localResultVisitSales2.getId_perusahaan());
        localResultVisitSales1.setPerusahaan(localResultVisitSales2.getPerusahaan());
        localResultVisitSales1.setId_kabupaten(localResultVisitSales2.getId_kabupaten());
        localResultVisitSales1.setId_provinsi(localResultVisitSales2.getId_provinsi());
        localResultVisitSales1.setId_segment(localResultVisitSales2.getId_segment());
        localResultVisitSales1.setOil_clinic(localResultVisitSales2.getOil_clinic());
        localResultVisitSales1.setPelatihan(localResultVisitSales2.getPelatihan());
        localResultVisitSales1.setGathering(localResultVisitSales2.getGathering());
        return localResultVisitSales1;
    }

}
