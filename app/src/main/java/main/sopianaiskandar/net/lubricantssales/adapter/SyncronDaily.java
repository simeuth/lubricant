package main.sopianaiskandar.net.lubricantssales.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Hashtable;
import java.util.Map;

import io.realm.RealmResults;
import main.sopianaiskandar.net.lubricantssales.Config;
import main.sopianaiskandar.net.lubricantssales.HomeActivity;
import main.sopianaiskandar.net.lubricantssales.LoginActivity;
import main.sopianaiskandar.net.lubricantssales.Parser;
import main.sopianaiskandar.net.lubricantssales.fragment.FragmentHome;
import main.sopianaiskandar.net.lubricantssales.model.Cluster;
import main.sopianaiskandar.net.lubricantssales.model.Equipment;
import main.sopianaiskandar.net.lubricantssales.model.Industry;
import main.sopianaiskandar.net.lubricantssales.model.Item;
import main.sopianaiskandar.net.lubricantssales.model.MasterEquipment;
import main.sopianaiskandar.net.lubricantssales.model.Provinsi;
import main.sopianaiskandar.net.lubricantssales.model.Region;
import main.sopianaiskandar.net.lubricantssales.model.ResultVisitSales;
import main.sopianaiskandar.net.lubricantssales.model.Rute;
import main.sopianaiskandar.net.lubricantssales.model.SalesProfile;
import main.sopianaiskandar.net.lubricantssales.model.Segment;

/**
 * Created by sopianaiskandar on 4/10/16.
 */
public class SyncronDaily {

    Activity act;
    FragmentHome home;

    public SyncronDaily(Activity act, FragmentHome home) {
        this.act = act;
        this.home = home;
    }

    public void syncronData() {
        SessionManager session = new SessionManager(act);
        String id_personal = session.getCanvaserID();
        String url = Config.API_URL + "/api-sinkron?id_personal=" + id_personal;
        final ProgressDialog loading = ProgressDialog.show(this.act, "Mencari rute...", "Please wait...", false, false);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        Log.i("JSON VISIT", s);
                        deleteAll();
                        try {
                            JSONObject S = new JSONObject(s);
                            try {

                                JSONArray product = S.getJSONArray("product");
                                saveDataProduct(product);

                                JSONArray rute = S.getJSONArray("routes");
                                saveDataRoutes(rute);

                                JSONArray visit = S.getJSONArray("visit");
                                saveDataVisit(visit);

                                JSONArray provinsi = S.getJSONArray("provinsi");
                                saveDataProvinsi(provinsi);

                                JSONArray jenis_industri = S.getJSONArray("jenis_industri");
                                saveDataIndustri(jenis_industri);

                                JSONArray segment = S.getJSONArray("segment");
                                saveDataSegment(segment);

                                JSONArray kabupaten = S.getJSONArray("kabupaten");
                                saveDataKabupaten(kabupaten);

                                home.dashboard();
                                home.getTitle();
                            } catch (Exception e) {
                                e.printStackTrace();
                                Log.d("respon", "Gagal Mengambil Data");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        loading.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        loading.dismiss();
                        Toast.makeText(act, "error", Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(act);
        requestQueue.add(stringRequest);
    }


    public void saveDataProduct(JSONArray response) {
        try {
            for (int i = 0; i < response.length(); i++) {
                JSONObject outlet = (JSONObject) response.get(i);
                HomeActivity.realm.beginTransaction();
                MasterEquipment item = HomeActivity.realm.createObject(MasterEquipment.class);
                item.setId(outlet.getInt("id"));
                item.setNama(outlet.getString("nama"));
                HomeActivity.realm.commitTransaction();
            }
        } catch (Exception e) {
            HomeActivity.realm.cancelTransaction();
        }
    }

    public void saveDataRoutes(JSONArray paramJSONArray) {
        try {
            JSONObject localJSONObject = (JSONObject) paramJSONArray.get(0);
            SessionManager localSessionManager = new SessionManager(this.act);
            HomeActivity.realm.beginTransaction();
            SalesProfile localSalesProfile = HomeActivity.realm.createObject(SalesProfile.class);
            localSalesProfile.setKodeSales(localSessionManager.getCanvaserID());
            localSalesProfile.setNamaSales(localSessionManager.getCluster());
            localSalesProfile.setTanggal(localJSONObject.getString("tanggal"));
            HomeActivity.realm.commitTransaction();
            return;
        } catch (Exception localException) {
            HomeActivity.realm.cancelTransaction();
        }
    }


    public void saveDataProvinsi(JSONArray paramJSONArray) {
        try {
            for (int i = 0; i < paramJSONArray.length(); i++) {
                JSONObject data = (JSONObject) paramJSONArray.get(i);
                HomeActivity.realm.beginTransaction();
                Provinsi item = HomeActivity.realm.createObject(Provinsi.class);
                item.setId(data.getInt("id"));
                item.setNama(data.getString("Nama"));
                HomeActivity.realm.commitTransaction();
            }
        } catch (Exception e) {
            HomeActivity.realm.cancelTransaction();
        }
    }

    public void saveDataKabupaten(JSONArray paramJSONArray) {
        try {
            for (int i = 0; i < paramJSONArray.length(); i++) {
                JSONObject data = (JSONObject) paramJSONArray.get(i);
                HomeActivity.realm.beginTransaction();
                Region item = HomeActivity.realm.createObject(Region.class);
                item.setId(data.getInt("id"));
                item.setId_provinsi(data.getInt("id_provinsi"));
                item.setName(data.getString("name"));
                HomeActivity.realm.commitTransaction();
            }
        } catch (Exception e) {
            HomeActivity.realm.cancelTransaction();
        }
    }


    public void saveDataIndustri(JSONArray paramJSONArray) {
        try {
            for (int i = 0; i < paramJSONArray.length(); i++) {
                JSONObject data = (JSONObject) paramJSONArray.get(i);
                HomeActivity.realm.beginTransaction();
                Industry item = HomeActivity.realm.createObject(Industry.class);
                item.setId(data.getInt("id"));
                item.setNama(data.getString("nama"));
                HomeActivity.realm.commitTransaction();
            }
        } catch (Exception e) {
            HomeActivity.realm.cancelTransaction();
        }
    }


    public void saveDataSegment(JSONArray paramJSONArray) {
        try {
            for (int i = 0; i < paramJSONArray.length(); i++) {
                JSONObject data = (JSONObject) paramJSONArray.get(i);
                HomeActivity.realm.beginTransaction();
                Segment item = HomeActivity.realm.createObject(Segment.class);
                item.setId(data.getInt("id"));
                item.setId_jenis_industri(data.getInt("id_jenis_industri"));
                item.setNama(data.getString("nama"));
                HomeActivity.realm.commitTransaction();
            }
        } catch (Exception e) {
            HomeActivity.realm.cancelTransaction();
        }
    }


    public void deleteAll() {
        deleteDataVisit();
        deleteProfile();
        deleteEquipment();
        jenisIndustriDelete();
        provinsiDelete();
        kabupatenDelete();
        segmentDelete();
    }

    public void deleteEquipment() {
        try {
            RealmResults<Equipment> realmResults = HomeActivity.realm.where(Equipment.class).findAll();
            HomeActivity.realm.beginTransaction();
            realmResults.clear();
            HomeActivity.realm.commitTransaction();
        } catch (Exception e) {
            HomeActivity.realm.cancelTransaction();
        }
    }


    public void provinsiDelete() {
        try {
            RealmResults<Provinsi> realmResults = HomeActivity.realm.where(Provinsi.class).findAll();
            HomeActivity.realm.beginTransaction();
            realmResults.clear();
            HomeActivity.realm.commitTransaction();
        } catch (Exception e) {
            HomeActivity.realm.cancelTransaction();
        }
    }

    public void kabupatenDelete() {
        try {
            RealmResults<Region> realmResults = HomeActivity.realm.where(Region.class).findAll();
            HomeActivity.realm.beginTransaction();
            realmResults.clear();
            HomeActivity.realm.commitTransaction();
        } catch (Exception e) {
            HomeActivity.realm.cancelTransaction();
        }
    }

    public void jenisIndustriDelete() {
        try {
            RealmResults<Industry> realmResults = HomeActivity.realm.where(Industry.class).findAll();
            HomeActivity.realm.beginTransaction();
            realmResults.clear();
            HomeActivity.realm.commitTransaction();
        } catch (Exception e) {
            HomeActivity.realm.cancelTransaction();
        }
    }

    public void segmentDelete() {
        try {
            RealmResults<Segment> realmResults = HomeActivity.realm.where(Segment.class).findAll();
            HomeActivity.realm.beginTransaction();
            realmResults.clear();
            HomeActivity.realm.commitTransaction();
        } catch (Exception e) {
            HomeActivity.realm.cancelTransaction();
        }
    }


    public void deleteProfile() {
        try {
            RealmResults<SalesProfile> realmResults = HomeActivity.realm.where(SalesProfile.class).findAll();
            HomeActivity.realm.beginTransaction();
            realmResults.clear();
            HomeActivity.realm.commitTransaction();
        } catch (Exception e) {
            HomeActivity.realm.cancelTransaction();
        }
    }

    public void saveDataVisit(JSONArray response) {
        try {
            for (int i = 0; i < response.length(); i++) {
                JSONObject localJSONObject = (JSONObject) response.get(i);
                HomeActivity.realm.beginTransaction();
                ResultVisitSales localResultVisitSales = HomeActivity.realm.createObject(ResultVisitSales.class);
                localResultVisitSales.setId_visit(localJSONObject.getInt("id_visit_perusahaan"));
                localResultVisitSales.setId_rute(localJSONObject.getInt("id_route"));
                localResultVisitSales.setLat(localJSONObject.getString("lat"));
                localResultVisitSales.setLon(localJSONObject.getString("lon"));
                localResultVisitSales.setCreated_at(localJSONObject.getString("created_at"));
                localResultVisitSales.setRecord_number(localJSONObject.getString("record_number"));
                localResultVisitSales.setPerusahaan(localJSONObject.getString("perusahaan"));
                localResultVisitSales.setAlamat(localJSONObject.getString("alamat"));
                localResultVisitSales.setContact(localJSONObject.getString("contact"));
                localResultVisitSales.setNo_hp(localJSONObject.getString("no_hp"));
                localResultVisitSales.setId_provinsi(localJSONObject.getInt("id_provinsi"));
                localResultVisitSales.setId_kabupaten(localJSONObject.getInt("id_kabupaten"));
                localResultVisitSales.setId_segment(localJSONObject.getInt("id_segment"));
                localResultVisitSales.setId_distributor(localJSONObject.getInt("id_distributor"));
                localResultVisitSales.setOil_clinic(localJSONObject.getString("oil_clinic"));
                localResultVisitSales.setPelatihan(localJSONObject.getString("pelatihan"));
                localResultVisitSales.setGathering(localJSONObject.getString("gathering"));
                localResultVisitSales.setId_region(localJSONObject.getInt("id_region"));
                localResultVisitSales.setId_perusahaan(localJSONObject.getInt("id_perusahaan"));
                localResultVisitSales.setId_jenis_industri(localJSONObject.getInt("id_jenis_industri"));
                localResultVisitSales.setIsSent(0);
                localResultVisitSales.setIs_visit("0");

//                localResultVisitSales.setId_visit(1);
//                localResultVisitSales.setId_rute(1);
//                localResultVisitSales.setLat("");
//                localResultVisitSales.setLon("");
//                localResultVisitSales.setCreated_at("");
//                localResultVisitSales.setRecord_number("");
//                localResultVisitSales.setPerusahaan("dummy");
//                localResultVisitSales.setAlamat("dummy");
//                localResultVisitSales.setContact("");
//                localResultVisitSales.setNo_hp("");
//                localResultVisitSales.setId_provinsi(1);
//                localResultVisitSales.setId_kabupaten(7);
//                localResultVisitSales.setId_segment(2);
//                localResultVisitSales.setId_distributor(1);
//                localResultVisitSales.setOil_clinic("0");
//                localResultVisitSales.setPelatihan("0");
//                localResultVisitSales.setGathering("0");
//                localResultVisitSales.setId_region(1);
//                localResultVisitSales.setId_perusahaan(1);
//                localResultVisitSales.setId_jenis_industri(2);
//                localResultVisitSales.setIsSent(0);
//                localResultVisitSales.setIs_visit("0");
                HomeActivity.realm.commitTransaction();
            }
        } catch (Exception e) {
            HomeActivity.realm.cancelTransaction();
        }
    }

    public void deleteDataVisit() {
        try {
            RealmResults<ResultVisitSales> realmResults = HomeActivity.realm.where(ResultVisitSales.class).findAll();
            HomeActivity.realm.beginTransaction();
            realmResults.clear();
            HomeActivity.realm.commitTransaction();
        } catch (Exception e) {
            HomeActivity.realm.cancelTransaction();
        }
    }
}
